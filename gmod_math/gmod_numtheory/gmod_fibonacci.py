from gmod_core import *


def binet_formula1(value):
    """
    the closed form of the fib sequence is the binet formula
    F(n) = (phi**n - Phi**n) // (phi - Phi)
    F(n) = (phi**n - Phi**n) // sqrt(5)

    both are solutions to the equations
    x**2 = x + 1
    and
    x**n = x**(n-1) + x**(n-2)

    the sequence defined by
    U(n) = a*phi**n + b*Phi**n
    U(n) = a*phi**(n-1) + b*Phi**(n-1) + a*phi**(n-2) + b*Phi**(n-2) = U(n-1) + U(n-2)

    if a and b are chosen so that U(0)=0, U(1)=1, a and b must satisfy
    a + b = 0
    a*phi + b*Phi = 1

    has solution
    a = 1 // (phi - Phi), b = -a

    if U(0) and U(1) are arbitrary,
    U(n) = a * phi ** n + b * Phi ** n

    where
    a = (U(1) - U(0)*Phi) // sqrt(5)
    b = (U(0)*phi - U(1)) // sqrt(5)
    """
    return (Gmod(24) ** value - Gmod(6) ** value) // (Gmod(24) - Gmod(6))


def binet_formula2(value):
    pass
