from gmod_trig import *

I_BASIS = GmodMatrix(1, 12, 17)
PHI_BASIS = GmodMatrix(1, 5, 23, 6, 24)
MASTER_I_MAT = GmodMatrix([I_BASIS * gmod for gmod in Gmod.ALL_GMODS])
MASTER_PHI_MAT = GmodMatrix([PHI_BASIS * gmod for gmod in Gmod.ALL_GMODS])


# MASTER_PHI_TRANS_MAT = GmodMatrix([gmod_golden_ratio.phi_trans_map(gmod) for gmod in Gmod.ALL_GMODS])


class GmodWordRegistry(dict):

    def __init__(self, matrix: GmodMatrix):
        super().__init__(word_matrix=matrix)

    def register_function(self, func):
        self[func.__name__] = func(self)

    def __str__(self):
        mystr = ""
        for k, v in self.items():
            mystr += str(k) + ":\n" + str(v) + "\n\n"

        return mystr

    def __repr__(self):
        return str(self)


"""
unit_circ_2x2 = GmodMatrix.collapse_4_to_2x2(GmodGeometry.A2.UNIT_CIRCLE.point_matrix)
for unit_circ_mat in unit_circ_2x2:
    new_poly_mat = [GmodPoly.poly_with_solutions(*new_circ_mat[0]),
                    GmodPoly.poly_with_solutions(*new_circ_mat[1])]
    euler_complexs = [[GmodComplex(unit_circ_row[0], unit_circ_row[2]), GmodComplex(unit_circ_row[0], unit_circ_row[3])],
                      [GmodComplex(unit_circ_row[1], unit_circ_row[2]), GmodComplex(unit_circ_row[1], unit_circ_row[3])]]

    euler_eval = list()
    for eulerc in euler_complexs:
        euler_eval.append([eulerc[0](12), eulerc[0](17)])

    euler_eval = GmodMatrix(euler_eval)

    unit_perms = [[unit_circ_row[0], unit_circ_row[2]],
                  [unit_circ_row[0], unit_circ_row[3]],
                  [unit_circ_row[1], unit_circ_row[2]],
                  [unit_circ_row[1], unit_circ_row[3]]]

            for perm in unit_perms:
                permcomp = GmodComplex(perm[0], perm[1])
                #unit_struct = [*perm, permcomp(12), permcomp(17)]
                unit_struct12 = [permcomp(12), permcomp, Gmod(12)]
                unit_struct17 = [permcomp(17), permcomp, Gmod(17)]
                circ_struct_list.append(unit_struct12)
                circ_struct_list.append(unit_struct17)

                if gmod == unit_struct12[0]:
                    gmod_comps.append(permcomp)
                if gmod == unit_struct17[0]:
                    gmod_comps.append(permcomp)

                #print(unit_struct12)
                #print(unit_struct17)
                #print()
        #print()

        return gmod_comps
        #print(circ_struct_list)

            #print(new_circ_mat)
            #print()
            #print(euler_eval)
            #print()
            #print(euler_complexs)
            #print()
            #print()
"""
