import fractions
import itertools
import math
import mpmath
import numpy as np
import pickle
import sympy
from mod import Mod


def print_separator():
    print(), print("********************************************************************************"), print()


gmod_mpcontext = mpmath.MPContext()
DEFAULT_CONVERT = gmod_mpcontext.convert
DEFAULT_MPC = gmod_mpcontext.mpc
DEFAULT_MATRIX = gmod_mpcontext.matrix
gmod_mpcontext.pretty = True
gmod_mpcontext.prec = 256


def gmod_mpmath_convert(value):
    if isinstance(value, (Gmod, GmodMatrix)):
        return value
    elif isinstance(value, (list, gmod_mpcontext.matrix)):
        return value
    else:
        value = DEFAULT_CONVERT(value)

        if isinstance(value, gmod_mpcontext.mpf):
            return Gmod(GematriaPrimus.float_to_gmod_int(value))
        elif isinstance(value, gmod_mpcontext.mpc):
            return Gmod(value)
        elif isinstance(value, gmod_mpcontext.matrix):
            return GmodMatrix(value)
        else:
            raise NotImplementedError("Cannot convert " + type(value))


class GematriaPrimus:
    GEMATRIA_LENGTH = 29
    FFIELD = sympy.PythonFiniteField(29)

    GEMATRIA_LETTERS = ['F', 'U', 'TH', 'O', 'R', 'C/K', 'G', 'W', 'H', 'N', 'I', 'J', 'EO', 'P', 'X', 'S/Z', 'T',
                        'B', 'E', 'M', 'L', 'NG/ING', 'OE', 'D', 'A', 'AE', 'Y', 'IA/IO', 'EA']
    GEMATRIA_PRIMES = [2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97,
                       101, 103, 107, 109]

    LEAST_PRIMITIVE_ROOT = 2

    BYTES_TYPES = (bytes,)
    FLOAT_TYPES = (float, sympy.numbers.Float, np.float128, mpmath.mpf, gmod_mpcontext.mpf)
    INT_TYPES = (int, Mod, np.int64, np.uint64, sympy.numbers.Integer)
    COMPLEX_TYPES = (complex, np.complex_, mpmath.mpc)
    LIST_TYPES = (list, np.ndarray)
    POLY_TYPES = (np.polynomial.Polynomial,)

    @staticmethod
    def prime_to_index(prime) -> int:
        """ return the index of a gprime in the Gematria """
        return GematriaPrimus.GEMATRIA_PRIMES.index(prime) + 1

    @staticmethod
    def index_to_letter(index) -> str:
        index = (index - 1) % GematriaPrimus.GEMATRIA_LENGTH
        return GematriaPrimus.GEMATRIA_LETTERS[index]

    @staticmethod
    def index_to_prime(index) -> int:
        index = (index - 1) % GematriaPrimus.GEMATRIA_LENGTH
        return GematriaPrimus.GEMATRIA_LETTERS[index]

    @staticmethod
    def gmod_hash(*args) -> int:
        struct_hash = 0
        degree = 0
        for self_next in args:
            next_step = GematriaPrimus.GEMATRIA_LENGTH ** degree

            struct_hash += next_step * int(Gmod(self_next))
            degree += 1
        return struct_hash

    @staticmethod
    def bytes_to_gmod_int(some_bytes: bytes) -> int:
        return Gmod(int.from_bytes(some_bytes, byteorder='little'))

    @staticmethod
    def float_to_gmod_int(some_float) -> int:
        some_float = some_float % 29
        frac_part = some_float % 1
        float_parts = (frac_part, some_float - frac_part)

        if math.isnan(float_parts[0]) or math.isnan(float_parts[1]):
            return 0

        dec_left = GematriaPrimus.FFIELD(float_parts[1])

        dec_frac = fractions.Fraction(float(float_parts[0])).limit_denominator()
        dec_frac_numer = GematriaPrimus.FFIELD(dec_frac.numerator)
        dec_frac_denom = GematriaPrimus.FFIELD(dec_frac.denominator)
        # right_of_dec = int(Gmod(right_of_dec_numer) // Gmod(right_of_dec_denom))
        if dec_frac_denom == 0:
            return 0

        dec_right = dec_frac_numer / dec_frac_denom

        return (dec_left + dec_right).val

    @staticmethod
    def prime_to_gmod_int(someprime) -> int:
        """ get gMod29 for single gPrime value """
        return GematriaPrimus.prime_to_index(someprime)

    @staticmethod
    def convert_to_gmod_int(value):
        if value is None:
            return 0
        elif isinstance(value, Gmod):
            return int(value) % GematriaPrimus.GEMATRIA_LENGTH
        elif isinstance(value, GematriaPrimus.BYTES_TYPES):
            return GematriaPrimus.bytes_to_gmod_int(value)
        elif isinstance(value, GematriaPrimus.FLOAT_TYPES):
            return GematriaPrimus.float_to_gmod_int(value)
        elif isinstance(value, GematriaPrimus.INT_TYPES):
            return int(value) % GematriaPrimus.GEMATRIA_LENGTH
        else:
            raise NotImplementedError("Cannot convert " + str(value) + "::" + str(type(value)) + " to gmod")


class Gmod(DEFAULT_MPC):
    ALL_GMODS = None
    NaN = 0
    pretty = True

    # def __init__(self, *args):
    #    pass
    # super().__init__(real=GematriaPrimus.convert_to_gmod_int(args[0]), imag=GematriaPrimus.convert_to_gmod_int(args[1]))
    # self.__setattr__("real", GematriaPrimus.convert_to_gmod_int(args[0]))
    # self.__setattr__("imag", GematriaPrimus.convert_to_gmod_int(args[1]))
    # super().__init__(*args)
    # self.real = GematriaPrimus.convert_to_gmod_int(args[0])
    # self.imag = GematriaPrimus.convert_to_gmod_int(args[1])

    def __new__(cls, *args):
        if isinstance(args[0], mpmath.matrix):
            return args[0]

        dummy = super(Gmod, cls).__new__(Gmod, *args)
        if len(args) == 2 and isinstance(args[0], Gmod) and isinstance(args[1], Gmod):
            dummy = super(Gmod, cls).__new__(Gmod, int(args[0]), int(args[1]))

        foo = super(Gmod, cls).__new__(Gmod, GematriaPrimus.convert_to_gmod_int(dummy.real),
                                       GematriaPrimus.convert_to_gmod_int(dummy.imag))
        return foo

        #
        # if len(args) == 1:
        #     if isinstance(args[0], GematriaPrimus.COMPLEX_TYPES):
        #         return Gmod(args[0].real, args[0].imag)
        #     elif isinstance(args[0], sympy.Add):
        #         return Gmod(*args[0].as_real_imag())
        #     else:
        #         return Gmod(args[0], 0)
        # elif len(args) == 2:
        #     try:
        #         real = GematriaPrimus.convert_to_gmod_int(args[0])
        #         imag = GematriaPrimus.convert_to_gmod_int(args[1])
        #         foo = super(Gmod, cls).__new__(Gmod, real, imag)
        #         return foo
        #     except Exception:
        #         print()
        #         print(args)
        # else:
        #     raise NotImplementedError("Cannot convert " + str(args) + " to gmod")

    @property
    def gletter(self) -> str:
        return GematriaPrimus.index_to_letter(self.greal)

    @property
    def gprime(self) -> int:
        return GematriaPrimus.index_to_prime(self.greal)

    @property
    def greal(self):
        return Gmod(GematriaPrimus.convert_to_gmod_int(self.real))

    @property
    def gimag(self):
        return Gmod(GematriaPrimus.convert_to_gmod_int(self.imag))

    def __int__(self) -> int:
        return int(self.real)

    def __index__(self):
        return int(self)

    def __hash__(self):
        return GematriaPrimus.gmod_hash(self.greal, self.gimag)

    def __bool__(self):
        return True

    def __str__(self):
        def gmod_str(gmod_int):
            return "[" + str(int(gmod_int)) + " % " + Gmod(gmod_int).gletter + "]"

        if int(self.imag) == 0:
            return gmod_str(self.greal)
        elif int(self.real) == 0 and int(self.imag) != 0:
            return gmod_str(self.gimag) + "i"
        else:
            return gmod_str(self.real) + " + " + gmod_str(self.imag) + "i"

    @property
    def mod_repr(self) -> Mod:
        return Mod(self, GematriaPrimus.GEMATRIA_LENGTH)

    def __neg__(self):
        return Gmod(super().__neg__())

    def conjugate(self):
        return Gmod(super().conjugate())

    def __invert__(self):
        if self == 0:
            return Gmod.NaN
        else:
            return Gmod(self.mod_repr.inverse())

    def __add__(self, other):
        return Gmod(super().__add__(other))

    __radd__ = __add__

    def __sub__(self, other):
        return Gmod(super().__sub__(other))

    def __rsub__(self, other):
        return Gmod(super().__rsub__(other))

    def __mul__(self, other):
        if isinstance(other, GmodMatrix):
            return other.__mul__(self)
        else:
            return Gmod(super().__mul__(other))

    __rmul__ = __mul__

    def __truediv__(self, other):
        if other == 0:
            return Gmod.NaN
        return Gmod(super().__truediv__(other))

    def __rtruediv__(self, other):
        if self == 0:
            return Gmod.NaN
        return Gmod(super().__rtruediv__(other))

    __floordiv__ = __truediv__
    __rfloordiv__ = __rtruediv__

    def __pow__(self, other):
        return Gmod(super().__pow__(other))

    def __rpow__(self, other):
        return Gmod(super().__rpow__(other))

    def __call__(self, *args, **kwargs):
        return Gmod(self.real + self.imag * args[0])

    @property
    def ispace12(self):
        return self(12)

    @property
    def ispace17(self):
        return self(17)

    @property
    def ispace(self):
        return GmodMatrix(self.ispace12, self.ispace17)

    @property
    def complex_proportion(self):
        if self.gimag == 0:
            return Gmod.NaN
        else:
            return self.greal // self.gimag

    def __mod__(self, other):
        return int(self.real) % other

    def __and__(self, other):
        return Gmod(super().__and__(other))

    def __or__(self, other):
        return Gmod(super().__or__(other))

    def __xor__(self, other):
        return Gmod(super().__xor__(other))

    def __lshift__(self, other):
        return Gmod(super().__lshift__(other))

    def __rshift__(self, other):
        return Gmod(super().__rshift__(other))

    def sqrt(self):
        return GmodMatrix(*[gmod for gmod in Gmod.ALL_GMODS if gmod ** 2 == self])

    @staticmethod
    def fevery_print(func):
        eval_list = list()
        for gmod in Gmod.ALL_GMODS:
            geval = func(gmod)
            print(gmod)
            print(geval)
            print()
            eval_list.append(geval)
        return eval_list

    @staticmethod
    def gmod_i_repr_with_base(gmod, base):
        gmod = Gmod(gmod)
        base = Gmod(base)
        base_i1 = (base - gmod) * Gmod(12)
        base_i2 = (base - gmod) * Gmod(17)

        complex1 = complex(base, base_i1)
        complex2 = complex(base, base_i2)
        return np.array([complex1, complex2])

    @staticmethod
    def gmod_i_repr_with_i_base(gmod, i_base):
        gmod = Gmod(gmod)
        i_base = Gmod(i_base)
        base_real1 = gmod - (i_base * Gmod(12))
        base_real2 = gmod - (i_base * Gmod(17))

        complex1 = complex(base_real1, i_base)
        complex2 = complex(base_real2, i_base)
        return np.array([complex1, complex2])


class GmodMatrix(DEFAULT_MATRIX):

    @staticmethod
    def identity(length, *args):
        return GmodMatrix(np.identity(length))

    @staticmethod
    def zeros(*shape):
        if isinstance(shape[0], tuple):
            return GmodMatrix(np.zeros(shape[0]))
        else:
            return GmodMatrix(np.zeros(shape))

    @staticmethod
    def ones(*shape):
        if isinstance(shape[0], tuple):
            return GmodMatrix(np.ones(shape[0]))
        else:
            return GmodMatrix(np.ones(shape))

    @staticmethod
    def prime_list_to_struct(prime_list):
        """ get list of gMod29s for a list of gPrimes """
        return GmodMatrix([Gmod(GematriaPrimus.prime_to_gmod_int(prime)) for prime in prime_list])

    @staticmethod
    def flatten_arg(arg):
        if isinstance(arg, tuple):
            return arg

        if isinstance(arg, (mpmath.matrix, GmodMatrix, gmod_mpcontext.matrix)):
            return [GmodMatrix.flatten_arg(x) for x in arg.tolist()]
        elif isinstance(arg, (list, np.ndarray)):
            return [GmodMatrix.flatten_arg(x) for x in arg]
        else:
            return arg

    def __init__(self, *args, **kwargs):
        self.ctx.convert = gmod_mpmath_convert
        if isinstance(args[0], tuple):
            super().__init__(args[0], **kwargs)
        else:
            args = [GmodMatrix.flatten_arg(arg) for arg in args]

            if isinstance(args[0], list):  # at least 2d
                if isinstance(args[0][0], list) or len(args) == 1:
                    super().__init__(*args, **kwargs)
                else:
                    super().__init__(args, **kwargs)
            elif isinstance(args[0], tuple):  # make with shape
                super().__init__(*(args[0]), **kwargs)
            else:
                super().__init__(args, **kwargs)  # make row

        self.row_type = kwargs.get("row_type", GmodMatrix)

    @property
    def shape(self):
        return self.rows, self.cols

    @property
    def determinate(self) -> Gmod:
        return Gmod(np.linalg.det(self))

    def __getitem__(self, item):
        if isinstance(item, Gmod):
            item = int(item)
        try:
            superget = super().__getitem__(item)
            if isinstance(superget, mpmath.matrix):
                return self.row_type(superget)
            else:
                return superget
        except IndexError:
            superget = self[item, :]
            if not isinstance(superget, self.row_type):
                return self.row_type(*superget)
            else:
                return superget

    def tolist(self):
        if self.cols == 1:
            return [self[x] for x in range(self.rows)]
        else:
            return [[self[x, y] for y in range(self.cols)] for x in range(self.rows)]

    def __iter__(self):
        return self.tolist().__iter__()

    def __eq__(self, other) -> bool:
        if isinstance(other, Gmod):
            return other in np.array(self)
        elif isinstance(other, GmodMatrix):
            return np.array_equal(self, other)
        else:
            return False

    def __add__(self, other):
        return type(self)(super().__add__(other))

    def __sub__(self, other):
        return type(self)(super().__sub__(other))

    def __mul__(self, other):
        return type(self)(super().__mul__(other))

    def __pow__(self, other):
        return type(self)(super().__pow__(other))

    def __floordiv__(self, other):
        return type(self)(super().__floordiv__(other))

    def __truediv__(self, other):
        return type(self)(super().__truediv__(other))

    def reduce_to_index(self, index, value=Gmod(1)):
        value = Gmod(value)
        index = int(index)
        ratio = value // Gmod(self[index])
        return self * ratio

    def transpose(self):
        return GmodMatrix(super().transpose())

    def dot(self, other):
        return Gmod(mpmath.fdot(self, other))

    @staticmethod
    def find_solution(mat_input, mat_output):
        assert isinstance(mat_input, GmodMatrix) and isinstance(mat_output, GmodMatrix)

        trans_m = mat_output.shape[1]
        trans_n = mat_input.shape[1]
        trans_shape = tuple([trans_m, trans_n])
        trans_sq = trans_m * trans_n

        for possible_solution in itertools.combinations_with_replacement(Gmod.ALL_GMODS, trans_sq):
            test_solution = GmodMatrix(
                [[possible_solution[m_ind + n_ind] for m_ind in range(trans_m)] for n_ind in range(trans_n)])
            print(test_solution)
            if mat_input @ test_solution == mat_output:
                return test_solution

    @staticmethod
    def h_mat(struct):
        return GmodMatrix([struct])

    @staticmethod
    def v_mat(struct):
        return GmodMatrix.h_mat(struct).transpose()

    @staticmethod
    def flat_mat(struct):
        return GmodMatrix([struct for x in range(len(struct))])

    @staticmethod
    def spiked_mat(struct):
        return GmodMatrix.flat_mat(struct).transpose()

    @staticmethod
    def circ_mat(struct):
        return GmodMatrix([[struct[outer - inner] for inner in range(len(struct))] for outer in range(len(struct))])

    @staticmethod
    def r_circ_mat(struct):
        return GmodMatrix.circ_mat(struct)[::-1]

    # @staticmethod
    # def collapse_y_2_to_3(some_mat):
    #     counts = [len([row for row in some_mat if row[1] == ind_gmod]) for ind_gmod in Gmod.ALL_GMODS]
    #     highest_count = max(*[counts])
    #     new_mat = list()
    #     for gmod in Gmod.ALL_GMODS:
    #         all_ys = [row[1] for row in some_mat if row[0] == gmod]
    #         if len(all_ys) != 0:
    #             for x in range(len(all_ys), highest_count):
    #                 all_ys.append(Gmod.BLANK)
    #             new_mat.append(GmodMatrix(gmod, *all_ys))
    #
    #     return GmodMatrix(new_mat)
    #
    # @staticmethod
    # def collapse_x_3_to_4(some_mat):
    #     all_evals = list()
    #     for row in some_mat:
    #         if row[1::] not in all_evals:
    #             all_evals.append(row[1::])
    #
    #     new_mat = list()
    #     for some_eval in all_evals:
    #         all_xs = [row[0] for row in some_mat if row[1::] == some_eval]
    #         if len(all_xs) == 1:
    #             all_xs.append(Gmod.BLANK)
    #         new_mat.append(GmodMatrix(*all_xs, *some_eval))
    #
    #     return GmodMatrix(new_mat)
    #
    # @staticmethod
    # def collapse_4_to_2x2(some_mat):
    #     mat_list = list()
    #     for row in some_mat:
    #         mat_list.append()
    #     return [GmodMatrix([[row[0], row[1], [row[2], row[3]]])]


gmod_mpcontext.convert = gmod_mpmath_convert
# mpmath.matrix([0]).ctx.convert = gmod_mpmath_convert

# gmod_mpcontext.mpc = Gmod
# gmod_mpcontext.matrix = GmodMatrix


Gmod.ALL_GMODS = [Gmod(x) for x in range(GematriaPrimus.GEMATRIA_LENGTH)]
Gmod.BLANK = Gmod(0)


# GmodComplex.BLANK = GmodComplex(Gmod.BLANK, Gmod.BLANK)


class GmodProportionStruct(GmodMatrix):
    UNIT_CIRCLE_e_AT_INF = None
    ALL_UNIQUE_PROPORTIONS = None
    ALL_UNIQUE3_PROPORTIONS = None

    def __init__(self, *args):
        super().__init__(*args)
        assert len(self) > 1

    def __eq__(self, other):
        if not isinstance(other, GmodProportionStruct):
            return False

        if len(self) != len(other):
            return False

        length = len(self)
        for x in range(length):
            m1 = self[x]
            n1 = self[(x + 1) % length]
            m2 = other[x]
            n2 = other[(x + 1) % length]
            if m1 * n2 - m2 * n1 != 0:
                return False

        return True

    def __str__(self):
        return super().__str__().replace(",", ":")

    @staticmethod
    def unique_prop(gmod):
        return GmodProportionStruct(gmod, Gmod(1))


GmodProportionStruct.UNIT_CIRCLE_e_AT_INF = GmodProportionStruct(-1, 0)
GmodProportionStruct.ALL_UNIQUE_PROPORTIONS = [GmodProportionStruct.unique_prop(x) for x in Gmod.ALL_GMODS]
GmodProportionStruct.ALL_UNIQUE_PROPORTIONS.append(GmodProportionStruct(1, 0))
GmodProportionStruct.LOAD_UNIQUE3_POINTS = False
GmodProportionStruct.MAKE_UNIQUE3_POINTS = False
if GmodProportionStruct.LOAD_UNIQUE3_POINTS:
    GmodProportionStruct.ALL_UNIQUE3_PROPORTIONS = pickle.load(open("ALL_UNIQUE3_PROPORTIONS.cache", "rb"))
elif GmodProportionStruct.MAKE_UNIQUE3_POINTS:
    GmodProportionStruct.ALL_UNIQUE3_PROPORTIONS = list()
    for z in Gmod.ALL_GMODS:
        for y in Gmod.ALL_GMODS:
            for x in Gmod.ALL_GMODS:
                if not (x == y == z == 0):
                    new_point = GmodProportionStruct(x, y, z)
                    print("Checking " + str(new_point))
                    if not (new_point in GmodProportionStruct.ALL_UNIQUE3_PROPORTIONS):
                        GmodProportionStruct.ALL_UNIQUE3_PROPORTIONS.append(new_point)

    pickle.dump(GmodProportionStruct.ALL_UNIQUE3_PROPORTIONS, open("ALL_UNIQUE3_PROPORTIONS.cache", "wb"))
    GmodProportionStruct.ALL_UNIQUE3_PROPORTIONS = pickle.load(open("ALL_UNIQUE3_PROPORTIONS.cache", "rb"))
else:
    GmodProportionStruct.ALL_UNIQUE3_PROPORTIONS = list()


class GmodExpr:

    def __init__(self, gfunc):
        self.gfunc = gfunc

    @property
    def real_solutions(self):
        real_solutions = []
        for gmod in Gmod.ALL_GMODS:
            if self.gfunc(gmod) is True:
                real_solutions.append(gmod)

        return real_solutions
