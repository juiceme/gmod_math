from gmod_numtheory import *

# Cipher Primes of Segment 13.1
C = GmodMatrix.prime_list_to_struct(
    [89, 41, 47, 13, 2, 31, 97, 19, 23, 17, 41, 83, 2, 59, 67, 83, 109, 97, 107, 53, 17, 103, 109, 29, 5, 101, 11, 7,
     107, 71, 19, 47, 71, 103, 89, 61, 59, 11, 3, 37, 23, 101, 83, 107, 79, 19, 23, 53, 2, 107, 73, 5, 59, 31, 19, 59,
     7, 3, 29, 19, 71, 73, 53, 5, 109, 19, 37, 97, 37, 101, 17, 103, 71, 19, 89, 43, 107, 67, 37, 19, 67, 3, 23, 19, 79,
     31, 53, 101, 17, 37, 73])

# Guess of Plaintext Primes of Segment 13.1: "AN EPIPHANY"
P = GmodMatrix.prime_list_to_struct([97, 29, 67, 43, 31, 43, 23, 97, 29, 103])

# Predict key for plaintext
C_CUT = GmodMatrix([C[x] for x in range(len(P))])
K = C_CUT - P
PCK = GmodMatrix([P, C_CUT, K])

# Extras
YOU = GmodMatrix.prime_list_to_struct([103, 7, 3])
SEEK = GmodMatrix.prime_list_to_struct([53, 67, 67, 13])
FOUND = GmodMatrix.prime_list_to_struct([2, 7, 3, 29, 89])

CIPHER_REG = GmodWordRegistry(PCK)
EXTRA_REG = GmodWordRegistry(GmodMatrix([[*YOU, 0, 0],
                                         [*SEEK, 0],
                                         FOUND]))

# HOPE = GmodPointTriangle.triangle_with_dims(YOU[0][0], SEEK[0][0])
# HOPE2 = GmodPointTriangle.triangle_with_dims(31, 480)
