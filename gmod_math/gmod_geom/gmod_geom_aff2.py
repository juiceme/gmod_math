import matplotlib.pyplot as plt

from gmod_core import *


# import mplcursors
# from mpldatacursor import datacursor

class GmodPoly(np.polynomial.Polynomial):

    @staticmethod
    def gmod_to_assoc_poly(gmod):
        return GmodPoly([int(x) for x in bin(gmod)[2:]][::-1])

    @staticmethod
    def poly_to_str(poly, reverse=True):
        assert isinstance(poly, GmodPoly)
        polystr = ""

        if not reverse:
            degcounter = 0
            for coef in poly.gmod_coef:
                polystr += str(coef)

                if degcounter != 0:
                    polystr += "x"
                    if degcounter != 1:
                        polystr += "**" + str(degcounter)

                if degcounter != len(poly.gmod_coef) - 1:
                    polystr += " + "

                degcounter += 1
        else:
            degcounter = len(poly.gmod_coef)
            for coef in poly.gmod_coef[::-1]:
                degcounter -= 1
                polystr += str(coef)

                if degcounter != 0:
                    polystr += "x"
                    if degcounter != 1:
                        polystr += "**" + str(degcounter)

                if degcounter != 0:
                    polystr += " + "

        return polystr

    @staticmethod
    def poly_with_solutions(*args):
        totalpoly = GmodPoly(1)
        for arg in args:
            totalpoly = totalpoly * GmodPoly([-Gmod(arg), 1])

        return totalpoly

    def __str__(self) -> str:
        return GmodPoly.poly_to_str(self)

    def __repr__(self) -> str:
        return str(self)

    @property
    def gmod_coef(self):
        return GmodMatrix(*self.coef)

    @property
    def gmod_reduced_poly(self):
        return GmodPoly(self.gmod_coef)

    @property
    def poly_solutions(self):
        return GmodMatrix(*[gmod for gmod in Gmod.ALL_GMODS if self(gmod) == 0])

    @property
    def poly_factors(self):
        polydivs = GmodMatrix(*[GmodPoly.poly_with_solutions(gmod) for gmod in self.poly_solutions])
        totalpoly = self
        for polydiv in polydivs:
            totalpoly = totalpoly // polydiv

        if len(totalpoly.poly_solutions) != 0:
            return GmodMatrix(totalpoly, *polydivs, *totalpoly.poly_factors)
        else:
            return GmodMatrix(totalpoly, *polydivs)

    def __call__(self, value) -> Gmod:
        if self.gmod_coef == self.gmod_reduced_poly.gmod_coef:
            return Gmod(super().__call__(value))
        else:
            return self.gmod_reduced_poly.__call__(value)


class GmodGeometryObject(GmodExpr):

    @staticmethod
    def angle_to_tangle(angle_in_degs):
        tangle = Gmod(22) * (Gmod(angle_in_degs) // Gmod(90))
        return tangle

    @staticmethod
    def tangle_to_angle(tangle):
        tangle = Gmod(tangle)
        if tangle == 0:
            return 0

        tangle_dec = 1 / int(~tangle)
        return 360 * tangle_dec

    @property
    def to_gmod_matrix(self) -> GmodMatrix:
        raise NotImplementedError("Must override to_gmod_matrix")

    def __repr__(self):
        return str(self)

    def __plot_on_xy__(self, ax, style):
        pass


class GmodA2GeometryObject(GmodGeometryObject):

    @property
    def to_gmod_matrix(self):
        raise NotImplementedError("Must override to_gmod_matrix")

    def is_a2_incident_with_point(self, point) -> bool:
        assert isinstance(point, GmodA2Point)
        return point in self.to_gmod_matrix

    def points_a2_intersects_a2(self, other):
        assert isinstance(other, GmodA2GeometryObject)
        return GmodMatrix([point for point in self.to_gmod_matrix
                           if point in other.to_gmod_matrix])

    def __plot_on_xy__(self, ax, style):
        raise NotImplementedError("Must override __plot_on_xy__")


class GmodPoint(GmodMatrix):

    def __init__(self, *args):
        super().__init__(*args)

    @property
    def quadrance(self):
        quadr = Gmod(0)
        for x in self:
            quadr = quadr + x ** 2
        return quadr

    @property
    def to_vector(self):
        return GmodVector(*self)

    @property
    def permutations(self):
        return [type(self)(*perm) for perm in list(itertools.permutations([*self]))]

    @property
    def to_gmod_matrix(self):
        return GmodMatrix([self])

    def dif_vector(self, other):
        assert isinstance(other, GmodPoint)
        return GmodVector(*[self[x] - other[x] for x in range(len(self))])

    def quadrance_between(self, other):
        assert isinstance(other, GmodPoint)
        quadrance = Gmod(0)
        for x in range(len(self)):
            quadrance += (self[x] - other[x]) ** 2

        return quadrance


class GmodVector(GmodMatrix):

    def __init__(self, *args):
        super().__init__(*args)

    quadrance = GmodPoint.quadrance
    permutations = GmodPoint.permutations

    @property
    def to_point(self):
        return GmodPoint(*self)

    @property
    def to_gmod_matrix(self):
        return GmodMatrix([[x] for x in self])

    @property
    def __str__(self):
        return str(self.to_gmod_matrix)

    quadrance_between = GmodPoint.quadrance_between
    dif_vector = GmodPoint.dif_vector


class GmodA2Point(GmodPoint, GmodA2GeometryObject):
    x: Gmod
    y: Gmod

    def __init__(self, *args):
        super().__init__(*args)
        assert len(self) == 2
        self.x = self[0]
        self.y = self[1]

    def __plot_on_xy__(self, ax, style):
        # fig, ax = plt.subplots()
        x = int(self.x)
        y = int(self.y)
        mplpoint = ax.plot(x, y, style, label="x:{}\ny:{}".format(x, y))
        # datacursor(formatter='{label}'.format)
        # mplcursors.cursor(mplpoint)
        # self.axes.annotate('(%s, %s)' % xy, xy=xy, textcoords='data')

    @staticmethod
    def points_are_colinear(point1, point2, point3) -> bool:
        """ page 39 """
        assert isinstance(point1, GmodA2Point) and isinstance(point2, GmodA2Point) and isinstance(point2, GmodA2Point)

        x1 = point1.x
        x2 = point2.x
        x3 = point3.x
        y1 = point1.y
        y2 = point2.y
        y3 = point3.y
        return x1 * y2 - x1 * y3 + x2 * y3 - x3 * y2 + x3 * y1 - x2 * y1 == 0

    @staticmethod
    def colinear_line_for_points(point1, point2, point3):
        assert isinstance(point1, GmodA2Point) and isinstance(point2, GmodA2Point) and isinstance(point3, GmodA2Point)
        assert GmodA2Point.points_are_colinear(point1, point2, point3)

        coline = GmodA2Line.make_line(point1, point2)
        assert GmodA2Line.line_incident_with_point(coline, point1) and \
               GmodA2Line.line_incident_with_point(coline, point2) and \
               GmodA2Line.line_incident_with_point(coline, point3)
        return coline

    @staticmethod
    def satisfies_fibonacci_identity(point1, point2):
        assert isinstance(point1, GmodA2Point) and \
               isinstance(point2, GmodA2Point)
        x1 = point1.x
        x2 = point2.x
        y1 = point1.y
        y2 = point2.y
        return (x1 * y2 - x2 * y1) ** 2 + (x1 * x2 + y1 * y2) ** 2 == (x1 ** 2 + y1 ** 2) * (x2 ** 2 + y2 ** 2)

    def __str__(self):
        return str(self.as_gmod_matrix)

    @staticmethod
    def complex_to_point(gmod):
        return GmodA2Point(gmod.greal, gmod.gimag)


ALL_2D_POINTS = [GmodA2Point(x, y) for y in Gmod.ALL_GMODS for x in Gmod.ALL_GMODS]
ORIGIN_2D = GmodA2Point(0, 0)
Gmod.complex_to_point = GmodA2Point.complex_to_point


class GmodA2Vector(GmodVector):
    x: Gmod
    y: Gmod

    def __init__(self, *args):
        super().__init__(*args)
        assert len(self) == 2
        self.x = self[0]
        self.y = self[1]

    def is_perp_with(self, other):
        assert isinstance(other, GmodA2Vector)
        return self.x * other.x + self.y * other.y == 0

    def is_para_with(self, other):
        assert isinstance(other, GmodA2Vector)
        return self.x * other.y - other.x * self.y == 0

    @staticmethod
    def signed_area(vec1, vec2):
        assert isinstance(vec1, GmodA2Vector) and isinstance(vec2, GmodA2Vector)

        return GmodMatrix([vec1, vec2]).det()

    @staticmethod
    def quadrance_between(vec1, vec2):
        assert isinstance(vec1, GmodA2Vector) and isinstance(vec2, GmodA2Vector)
        quadrance = Gmod(0)
        for x in range(len(vec1)):
            quadrance = quadrance + (vec2[x] - vec1[x]) ** 2

        return quadrance

    @staticmethod
    def spread_betweens(vec1, vec2):
        assert isinstance(vec1, GmodA2Vector) and isinstance(vec2, GmodA2Vector)
        if vec1.vector_quadrance == 0 or vec2.vector_quadrance == 0:
            return False
        Q1 = vec1.vector_quadrance
        Q2 = vec2.vector_quadrance
        return Gmod(1) - (vec1.dot(vec2) ** 2 // (Q1 * Q2))


class GmodA2Line(GmodProportionStruct, GmodA2GeometryObject):
    """ line defined as ax + by + c = 0 """

    @staticmethod
    def x_line(x):
        """ create a line that passes through all specified x values """
        return GmodA2Line(1, 0, x)

    @staticmethod
    def y_line(y):
        """ create a line that passes through all specified y values """
        return GmodA2Line(0, 1, y)

    @staticmethod
    def line_coef_points(point1, point2):
        assert isinstance(point1, GmodA2Point) and isinstance(point1, GmodA2Point)
        """ page 38 """
        a = point1.y - point2.y
        b = point2.x - point1.x
        c = point1.x * point2.y - point2.x * point1.y

        return [a, b, -c]

    @staticmethod
    def line_coef_parametric(vector, point):
        assert isinstance(point, GmodA2Point) and isinstance(vector, GmodA2Vector)
        # page 38
        a = point.y - vector.y
        b = vector.x - point.x
        c = point.x * vector.y - vector.x * point.y
        return GmodA2Line(a, b, c)

    a: Gmod
    b: Gmod
    c: Gmod

    def __init__(self, *args):
        if len(args) == 3:
            super().__init__(*args)
            self.a = self[0]
            self.b = self[1]
            self.c = self[2]
        elif len(args) == 2:
            if isinstance(args[0], GmodA2Vector) and isinstance(args[1], GmodA2Point):
                GmodA2Line.__init__(*GmodA2Line.line_coef_parametric(args[0], args[1]))
            elif isinstance(args[0], GmodA2Point) and isinstance(args[1], GmodA2Point):
                GmodA2Line.__init__(*GmodA2Line.line_coef_points(args[0], args[1]))
            else:
                raise NotImplementedError
        else:
            raise NotImplementedError

    @property
    def x_intercept(self):
        if self.a == 0:
            return None
        else:
            return GmodA2Point(self.c // self.a, 0)

    @property
    def y_intercept(self):
        if self.b == 0:
            return None
        else:
            return GmodA2Point(0, self.c // self.b)

    @property
    def is_null_line(self):
        """ page 36 """
        return self.a ** 2 + self.b ** 2 == 0

    def is_incident_with_point(self, point):
        """ page 40 """
        assert isinstance(point, GmodA2Point)
        return self.a * point.x + self.b * point.y == self.c

    @property
    def direction_vector(self):
        if self.x_intercept is None:
            return GmodA2Vector(1, 0)
        elif self.y_intercept is None:
            return GmodA2Vector(0, 1)
        elif self.x_intercept == GmodA2Point.ORIGIN_2D or self.y_intercept == GmodA2Point.ORIGIN_2D:
            return self.solve_y(1).as_vector
        else:
            return self.y_intercept.vector_dif(self.x_intercept)

    @property
    def to_gmod_matrix(self):
        return GmodMatrix([point for point in GmodA2Point.ALL_2D_POINTS if self.is_incident_with_point(point)],
                          row_type=GmodA2Point)

    def solve_y(self, value):
        """
        :param value:
        :return: solve for y
        """
        value = Gmod(value)
        line_eval = GmodA2Point(value, (self.c - self.a * value) // self.b)
        assert GmodA2Line.line_incident_with_point(self, line_eval)
        return line_eval

    def line_parallel_at_point(self, point):
        """ page 41 """
        assert isinstance(point, GmodA2Point)
        a = self.a
        b = self.b
        c = -self.a * point.x - self.b * point.y
        return GmodA2Line(a, b, c)

    def altitude_line_of_point(self, point):
        """ page 41 """
        assert isinstance(point, GmodA2Point)
        a = -self.b
        b = self.a
        c = self.b * point.x - self.a * point.y
        return GmodA2Line(a, b, c)

    def foot_of_altitude(self, point):
        a = self.a
        b = self.b
        c = self.c
        x = point.x
        y = point.y
        new_x = (b ** 2 * x - a * b * y - a * c) / (a ** 2 + b ** 2)
        new_y = (-a * b * x + a ** 2 * y - b * c) / (a ** 2 + b ** 2)
        return GmodA2Point(new_x, new_y)

    def quadrance_to_point(self, point):
        a = self.a
        b = self.b
        c = self.c
        x = point.x
        y = point.y
        return (a * x + b * y + c) ** 2 / (a ** 2 + b ** 2)

    def __plot_on_xy__(self, ax, style):
        for x in range(len(self.to_gmod_matrix) - 1):
            curr_p = self.to_gmod_matrix[x]
            next_p = self.to_gmod_matrix[x + 1]
            xs = (int(curr_p.x), int(next_p.x))
            ys = (int(curr_p.y), int(next_p.y))
            ax.plot(xs, ys, "-" + str(style))

    @staticmethod
    def spread_of_lines(line1, line2):
        """ find the spread of two lines """
        assert isinstance(line1, GmodA2Line) and isinstance(line2, GmodA2Line)
        # return ((line1.a * line2.b - line2.a * line1.b)** 2) / ((line1.a**2 + line2.a**2) * (line1.b**2 + line2.b**2))
        return GmodA2Vector.spread_between_vectors(line1.direction_vector, line2.direction_vector)

    @staticmethod
    def lines_are_perp(line1, line2):
        """ page 40 """
        assert isinstance(line1, GmodA2Line) and isinstance(line2, GmodA2Line)
        return line1.a * line2.b - line1.b * line2.a == Gmod(0)

    @staticmethod
    def lines_are_para(line1, line2):
        """ page 40 """
        assert isinstance(line1, GmodA2Line) and isinstance(line2, GmodA2Line)
        return line1.a * line2.b + line1.b * line2.a == Gmod(0)

    @staticmethod
    def lines_are_concurrent(line1, line2, line3):
        """ page 39 """
        assert isinstance(line1, GmodA2Line) and isinstance(line2, GmodA2Line) and isinstance(line3, GmodA2Line)
        a1 = line1.a
        a2 = line2.a
        a3 = line3.a
        b1 = line1.b
        b2 = line2.b
        b3 = line3.b
        c1 = line1.c
        c2 = line2.c
        c3 = line3.c
        return (a1 * b2 * c3) - (a1 * b3 * c2) + (a2 * b3 * c1) - (a3 * b2 * c1) + (a3 * b1 * c2) - (a2 * b1 * c3) == 0

    @staticmethod
    def concurrent_point_of_lines(line1, line2, line3):
        """ page 39 """
        assert isinstance(line1, GmodA2Line) and isinstance(line2, GmodA2Line) and isinstance(line3, GmodA2Line)
        copoint = GmodA2Line.meet_of_lines(line1, line2)
        assert GmodA2Line.line_incident_with_point(line3, copoint)
        return copoint

    @staticmethod
    def meet_of_lines(line1, line2):
        """ page 40 """
        assert isinstance(line1, GmodA2Line) and isinstance(line2, GmodA2Line)
        a1 = line1.a
        a2 = line2.a
        b1 = line1.b
        b2 = line2.b
        c1 = line1.c
        c2 = line2.c
        x = (b1 * c2 - b2 * c1) / (a1 * b2 - a2 * b1)
        y = (c1 * a2 - c2 * a1) / (a1 * b2 - a2 * b1)
        return GmodA2Point(x, y)

    @property
    def __printdict__(self):
        line_dict = dict()
        line_dict["line"] = "" + str(int(self.a)) + "x + " + str(int(self.b)) + "y + " + str(int(self.c)) + " = 0"
        line_dict["is_null"] = str(self.is_null_line())
        line_dict["a"] = str(self.a)
        line_dict["b"] = str(self.b)
        line_dict["c"] = str(self.c)

        return line_dict

    def __str__(self):
        return str(GmodMatrix.h_mat(self))


X_AXIS = GmodA2Line(0, 1, 0)
Y_AXIS = GmodA2Line(1, 0, 0)
ALL_X_LINES = [GmodA2Line.x_line(x) for x in Gmod.ALL_GMODS]
ALL_Y_LINES = [GmodA2Line.y_line(y) for y in Gmod.ALL_GMODS]


class GmodA2Side(GmodMatrix, GmodA2GeometryObject):
    pointA: GmodA2Point
    pointB: GmodA2Point

    side_vector: GmodA2Vector
    side_line: GmodA2Line
    side_quadrance: Gmod
    side_midpoint: GmodA2Point

    def __init__(self, point1, point2):
        assert isinstance(point1, GmodA2Point) and isinstance(point2, GmodA2Point)
        super().__init__([point1, point2], row_type=GmodA2Point)
        self.pointA = point1
        self.pointB = point2
        self.side_vector = self.pointA.vector_dif(self.pointB)
        self.side_line = GmodA2Line(self.pointA, self.pointB)
        self.side_quadrance = GmodA2Point.quadrance_between(self.pointA, self.pointB)
        self.side_midpoint = (self.pointA + self.pointB) * Gmod(1 / 2)

    def __plot_on_xy__(self, ax, style):
        xs = (int(self.A1.x), int(self.A2.x))
        ys = (int(self.A1.y), int(self.A2.y))
        q = int(self.side_quadrance)
        ax.plot(xs, ys, "-" + str(style), label="Q:{}".format(q))

    @property
    def to_gmod_matrix(self):
        return GmodMatrix([self[0], self[1]])

    @property
    def side_info(self):
        self_dict = dict()
        self_dict["points"] = [point.__strrepr__ for point in self.side_points]
        self_dict["line"] = self.side_line.__strrepr__
        self_dict["Q"] = self.side_quadrance.__strrepr__

        return self_dict

    @staticmethod
    def sides_are_para(side1, side2):
        assert isinstance(side1, GmodA2Side) and isinstance(side2, GmodA2Side)
        return GmodA2Vector.vectors_are_para(side1.side_vector, side2.side_vector)

    @staticmethod
    def sides_are_perp(side1, side2):
        assert isinstance(side1, GmodA2Side) and isinstance(side2, GmodA2Side)
        return GmodA2Vector.vectors_are_perp(side1.side_vector, side2.side_vector)


class GmodA2Vertex:

    def __init__(self, line1, line2):
        assert isinstance(line1, GmodA2Line) and isinstance(line2, GmodA2Line)
        # assert self.line1 != self.line2
        self.l1 = line1
        self.l2 = line2

    @property
    def lines(self):
        return [self.l1, self.l2]

    @property
    def vertex_point(self):
        return GmodA2Line.meet_of_lines(self.lines[0], self.lines[1])

    @property
    def vertex_spread(self):
        return GmodA2Line.spread_of_lines(self.lines[0], self.lines[1])

    @property
    def is_null_vertex(self):
        return self.lines[0].is_null_line() or self.lines[1].is_null_line()

    @property
    def is_right_vertex(self):
        return self.lines[0].is_perp_with(self.lines[1])

    @property
    def __printdict__(self):
        self_dict = dict()
        self_dict["vertex"] = str(self.vertex_point)
        self_dict["l1"] = str(self.lines[0])
        self_dict["l2"] = str(self.lines[1])
        self_dict["S"] = str(self.vertex_spread)

        return self_dict


class GmodA2Ngon(GmodMatrix, GmodA2GeometryObject):

    def __init__(self, *args):
        assert len(args) >= 3
        for arg in args:
            assert isinstance(arg, GmodA2Point)
        args = list(args)
        super().__init__(args, row_type=GmodA2Point)

    def __plot_on_xy__(self, ax, style):
        for n_A in self.ngon_A_n:
            n_A.__plot_on_xy__(ax, style)
        for n_side in self.sides:
            n_side.__plot_on_xy__(ax, style)

    @property
    def sides(self):
        return [GmodA2Side(self[x % len(self)], self[(x + 1) % len(self)]) for x in range(len(self))]

    @property
    def quadrance_list(self):
        return GmodMatrix([side.side_quadrance for side in self.sides])

    @property
    def side_lines(self):
        return [side.side_line for side in self.sides]

    @property
    def vertices(self):
        length = len(self.side_lines)
        ngon_verts = list()
        for x in range(length):
            curr_L = self.side_lines[(x + 1) % length]
            next_L = self.side_lines[(x + 2) % length]
            curr_vert = GmodA2Vertex(curr_L, next_L)
            ngon_verts.append(curr_vert)
        return ngon_verts

    @property
    def spread_list(self):
        return GmodMatrix([vert.vertex_spread for vert in self.vertices])

    @property
    def cross_list(self):
        return GmodMatrix([Gmod(1) - spread for spread in self.spread_list])

    @property
    def side_midpoints(self):
        return GmodMatrix([side.side_midpoint for side in self.sides], row_type=GmodA2Point)

    @property
    def to_gmod_matrix(self):
        return self

    @property
    def ngon_info(self):
        print_dict = dict()

        print_dict["points"] = [str(point) for point in self]
        print_dict["lines"] = [str(line) for line in self.side_lines]
        print_dict["Q"] = [str(quadr) for quadr in self.quadrance_list]
        print_dict["s"] = [str(spread) for spread in self.spread_list]

        # print_dict["sides"] = [side.triangle_info for side in self.sides]
        # print_dict["vertices"] = [vert.triangle_info for vert in self.vertices]

        return print_dict


class GmodA2Triangle(GmodA2Ngon):

    def __init__(self, point_a, point_b, point_c):
        assert isinstance(point_a, GmodA2Point)
        assert isinstance(point_b, GmodA2Point)
        assert isinstance(point_c, GmodA2Point)

        super().__init__(point_a, point_b, point_c)

    @property
    def A(self):
        return self[0]

    @property
    def B(self):
        return self[1]

    @property
    def C(self):
        return self[2]

    @property
    def AB(self):
        return self.sides[0]

    @property
    def BC(self):
        return self.sides[1]

    @property
    def CA(self):
        return self.sides[2]

    @property
    def triple_quad_colinear(self):
        Q1 = self.quadrance_list[0]
        Q2 = self.quadrance_list[1]
        Q3 = self.quadrance_list[2]
        return (Q1 + Q2 + Q3) ** 2 == (Q1 ** 2 + Q2 ** 2 + Q3 ** 2) * 2

    @property
    def pyth_theorem_perp(self):
        Q1 = self.quadrance_list[0]
        Q2 = self.quadrance_list[1]
        Q3 = self.quadrance_list[2]
        return Q1 + Q2 == Q3

    @property
    def spread_law(self):
        Q1 = self.quadrance_list[0]
        Q2 = self.quadrance_list[1]
        Q3 = self.quadrance_list[2]
        s1 = self.spread_list[0]
        s2 = self.spread_list[1]
        s3 = self.spread_list[2]
        spread_law_eval = s1 // Q1 == s2 // Q2 == s3 // Q3
        if spread_law_eval:
            return True
        else:
            return str(s1) + "//" + str(Q1) + "!=" \
                   + str(s2) + "//" + str(Q2) + "!=" \
                   + str(s3) + "//" + str(Q3)

    @property
    def cross_law(self):
        Q1 = self.quadrance_list[0]
        Q2 = self.quadrance_list[1]
        Q3 = self.quadrance_list[2]
        c3 = self.cross_list[2]
        return (Q1 + Q2 - Q3) ** 2 == Q1 * Q2 * c3 * 4

    @property
    def triple_spread_law(self):
        s1 = self.spread_list[0]
        s2 = self.spread_list[1]
        s3 = self.spread_list[2]
        return (s1 + s2 + s3) ** 2 == (s1 ** 2 + s2 ** 2 + s3 ** 2) * 2 + s1 * s2 * s3 * 4

    @property
    def is_logistic_map(self):
        l1 = self.sides[0].side_line
        l2 = self.sides[1].side_line
        l3 = self.sides[2].side_line
        s = GmodA2Line.spread_of_lines(l1, l2)
        r = GmodA2Line.spread_of_lines(l1, l3)
        if r == s:
            log_map = (s * 4) * (-s + 1)
            if r == 0:
                return False
            else:
                return log_map
        return False

    @property
    def triangle_medians(self):
        assert len(self) == len(self)
        return [GmodA2Side(self[x], self[(x + 1) % len(self)]).side_midpoint
                for x in range(len(self))]

    @property
    def triangle_centroid(self):
        cent1 = GmodA2Line.meet_of_lines(self.triangle_medians[0].side_line, self.triangle_medians[1].side_line)
        cent2 = GmodA2Line.meet_of_lines(self.triangle_medians[1].side_line, self.triangle_medians[2].side_line)
        cent3 = GmodA2Line.meet_of_lines(self.triangle_medians[2].side_line, self.triangle_medians[0].side_line)
        assert cent1 == cent2 == cent3
        return cent1

    # noinspection PyTypeChecker
    @property
    def triangle_info(self):
        tri_dict = super().ngon_info

        # tri_dict["centroid"] = self.triangle_centroid.__strrepr__
        # tri_dict["medians"] = [median.triangle_info for median in self.triangle_medians]

        tri_dict["laws"] = dict()
        tri_dict["laws"]["TripleQuadColinear"] = str(self.triple_quad_colinear)
        tri_dict["laws"]["PythTheoremPerp"] = str(self.pyth_theorem_perp)
        tri_dict["laws"]["SpreadLaw"] = str(self.spread_law)
        tri_dict["laws"]["CrossLaw"] = str(self.cross_law)
        tri_dict["laws"]["TripleSpreadLaw"] = str(self.triple_spread_law)
        tri_dict["laws"]["IsLogisticMap"] = str(self.is_logistic_map)
        return tri_dict

    def thales_theorem(self, value):
        assert value != 0
        B2 = self.A * (-value + 1) + self.B * value
        B3 = self.A * (-value + 1) + self.C * value
        assert GmodA2Side.sides_are_para(self.BC, GmodA2Side(B2, B3))
        return GmodA2Triangle(self.A, B2, B3)

    @staticmethod
    def calc_spread_cross_law(q1, q2, q3):
        numer = (q1 + q2 - q3) ** 2
        denom = q1 * q2 * Gmod(4)
        spread = Gmod(1) - (numer // denom)
        return spread


UNIT_TRIANGLE = GmodA2Triangle(ORIGIN_2D, GmodA2Point(1, 0), GmodA2Point(0, 1))


class GmodA2Quad(GmodA2Ngon, GmodA2GeometryObject):
    """
        denotes a rectangle in the ffe with:
            point of bottom left of rect ["bot-l"]
            point of top right of rect ["top-r"]

       can accept GmodPoints
    """

    def __init__(self, A1, A2, A3, A4):
        super().__init__(A1, A2, A3, A4)

    @property
    def quad_diags(self):
        diag_AC = GmodA2Side(self.ngon_A_n[0], self.ngon_A_n[2])
        diag_BD = GmodA2Side(self.ngon_A_n[1], self.ngon_A_n[3])
        return [diag_AC, diag_BD]

    @property
    def is_parallelogram(self):
        return GmodA2Side.sides_are_para(self.sides[0], self.sides[2]) and \
               GmodA2Side.sides_are_para(self.sides[1], self.sides[3])

    @property
    def is_rectangle(self):
        return self.is_parallelogram and \
               GmodA2Side.sides_are_perp(self.quad_bisectors[0], self.quad_bisectors[1])

    @property
    def is_rhombus(self):
        return self.is_parallelogram and \
               GmodA2Side.sides_are_perp(self.quad_diags[0], self.quad_diags[1])

    @property
    def is_square(self):
        return self.is_rectangle and self.is_rhombus

    @property
    def quad_center(self):
        assert self.is_parallelogram()
        return self.quad_diags[0].side_line.intersects_with_line(self.quad_diags[1].side_line)

    @property
    def quad_bisectors(self):
        bisector1 = GmodA2Side(self.sides[0].side_midpoint, self.sides[2].side_midpoint)
        bisector2 = GmodA2Side(self.sides[1].side_midpoint, self.sides[3].side_midpoint)
        return [bisector1, bisector2]

    @property
    def midpoint_quad(self):
        """ page 49 (share the same center) """
        return GmodA2Quad(self.sides[0].side_midpoint,
                          self.sides[1].side_midpoint,
                          self.sides[2].side_midpoint,
                          self.sides[3].side_midpoint)

    @property
    def triangle_info(self):
        quad_dict = super().triangle_info
        quad_dict["diags"] = [quad.side_info for quad in self.quad_diags]
        quad_dict["IsParallelogram"] = str(self.is_parallelogram)
        quad_dict["IsRectangle"] = str(self.is_rectangle)
        quad_dict["IsRhombus"] = str(self.is_rhombus)
        quad_dict["IsSquare"] = str(self.is_square)
        return quad_dict


class GmodA2Conic(GmodProportionStruct, GmodA2GeometryObject):
    """ a conic representing
            d   e   f
              a    b
                 c
    """
    d: Gmod
    e: Gmod
    f: Gmod
    a: Gmod
    b: Gmod
    c: Gmod

    def __init__(self, d, e, f, a, b, c):
        super().__init__([d, e, f, a, b, c])
        self.d = self[0]
        self.e = self[1]
        self.f = self[2]
        self.a = self[3]
        self.b = self[4]
        self.c = self[5]

    @property
    def to_gmod_matrix(self):
        return GmodMatrix([[point.x, point.y] for point in ALL_2D_POINTS if self.is_conic_touching_point(point)],
                          row_type=GmodA2Point)

    @property
    def is_standard(self) -> bool:
        return self.is_conic_touching_point(GmodA2Point.ORIGIN_2D)

    @property
    def is_singular(self) -> bool:
        return self.a == self.b == 0

    @property
    def is_empty(self) -> bool:
        return len(self.to_gmod_matrix) == 0

    @property
    def has_center(self) -> bool:
        return self.e ** 2 - self.d * self.f * 4 != 0

    @property
    def conic_center(self):
        if not self.has_center:
            return None
        else:
            center_x = (self.a * self.f * 2 - self.b * self.e) / \
                       (self.e * self.e - self.d * self.f * 4)
            center_y = (self.b * self.d * 2 - self.a * self.e) / \
                       (self.e * self.e - self.d * self.f * 4)
            return GmodA2Point(center_x, center_y)

    def __plot_on_xy__(self, ax, style):
        for point in self.to_gmod_matrix:
            point.__plot_on_xy__(ax, style)

    def tangent_line_on_point(self, point) -> GmodA2Line:
        assert isinstance(point, GmodA2Point)
        assert self.is_conic_touching_point(point)

        a = self.a + self.d * point.x * 2 + self.e * point.y
        b = self.b + self.e * point.x + self.f * point.y * 2
        c = self.a * point.x + self.b * point.y + self.c * 2
        return GmodA2Line(a, b, c)

    def is_conic_touching_point(self, point) -> bool:
        assert isinstance(point, GmodA2Point)
        c1 = self.d * point.x * point.x
        c2 = self.e * point.x * point.y
        c3 = self.f * point.y * point.y
        c4 = self.a * point.x
        c5 = self.b * point.y
        c6 = self.c
        return c1 + c2 + c3 + c4 + c5 + c6 == 0


CHAOS_THEORY_PARAB = GmodA2Conic(4, 0, 0, -4, 1, 0)


class GmodA2Circle(GmodA2Conic):
    """
    denotes a circle with a centerpoint and a quadrance radius

    conic representing circle on page 171
    """

    @staticmethod
    def with_center_and_radius(center, radius):
        assert isinstance(center, GmodA2Point)
        radius = Gmod(radius)
        h = center.x
        k = center.y
        d = Gmod(1)
        e = Gmod(0)
        f = Gmod(1)
        a = -h * 2
        b = -k * 2
        c = h * h + k * k - radius
        return [d, e, f, a, b, c]

    def __init__(self, *args):
        if len(args) == 6:
            super().__init__(*args)
        else:
            super().__init__(*GmodA2Circle.with_center_and_radius(args[0], args[1]))

    @property
    def center(self):
        return GmodA2Point(-self.a // 2, -self.b // 2)

    @property
    def radius(self):
        return self.center.x ** 2 + self.center.y ** 2 - self.c

    @property
    def discrete_polys(self):
        return GmodPoly.poly_with_solutions(self.center.x) ** 2, GmodPoly.poly_with_solutions(self.center.y) ** 2

    @property
    def discrete_poly_combo(self):
        return self.discrete_polys[0] + self.discrete_polys[1]

    def __add__(self, other):
        if isinstance(other, GmodA2Point):
            return GmodA2Circle(self.center + other, self.radius)
        else:
            return super().__add__(other)

    def __mul__(self, other):
        super_mul = super().__mul__(other)
        return GmodA2Circle(1, 0, 1, super_mul.a, super_mul.b, super_mul.c)

    @staticmethod
    def unit_circ_e(self):
        x = (Gmod(1) - self ** 2) // (Gmod(1) + self ** 2)
        y = (Gmod(2) * self) // (Gmod(1) + self ** 2)
        return GmodProportionStruct(x, y)

    @staticmethod
    def proj_circ_e(self):
        unit_params = self.unit_circ_e()
        return unit_params.normalize()

    @staticmethod
    def unit_circ_cos(self):
        return self.unit_circ_e()[0]

    @staticmethod
    def unit_circ_sin(self):
        return self.unit_circ_e()[1]


UNIT_CIRCLE = GmodA2Circle(ORIGIN_2D, 1)
ROJECTIVE_CIRCLE = GmodA2Circle(GmodA2Point(0, 1 / 2), 1 / 4)
ALL_ORIGIN_CIRCLES = [GmodA2Circle(ORIGIN_2D, radius) for radius in Gmod.ALL_GMODS]


class GmodA2Parabola(GmodA2Conic):
    """
    represents a parabola with
        point of focus - [u,v]
        line directrix - <a:b:c>

    conic representing parabola on page 173
    """

    def __new__(cls, focus, directrix):
        assert isinstance(focus, GmodA2Point) and isinstance(directrix, GmodA2Line)
        d = directrix.b ** 2
        e = directrix.a * directrix.b * -2
        f = directrix.a ** 2
        a = -(directrix.a * directrix.c * 2) - \
            ((directrix.a ** 2 + directrix.b ** 2) * focus.x * 2)
        b = -(directrix.b * directrix.c * 2) - \
            ((directrix.a ** 2 + directrix.b ** 2) * focus.y * 2)
        c = (directrix.a ** 2 + directrix.b ** 2) * \
            (focus.x ** 2 + focus.y ** 2) - directrix.c ** 2
        conic = GmodA2Conic.__new__(cls, d, e, f, a, b, c)
        conic.focus = focus
        conic.directrix = directrix
        return conic

    @property
    def __printdict__(self):
        para_dict = super().__printdict__
        para_dict["focus"] = self.focus.__strrepr__
        para_dict["directrix"] = self.directrix.__strrepr__
        return para_dict


class GmodA2Transform:

    def __init__(self, *args):
        """

        :param args:GmodHPoint,GmodHLine
        """
        if len(args) == 1:
            trans = args[0]
            if isinstance(trans, GmodA2Point):
                self.a = trans
                self.type = "point-transform"
            elif isinstance(trans, GmodA2Line):
                self.L = trans
                self.type = "line-transform"
            elif isinstance(trans, list):
                self.affine = trans
                self.type = "value-transform"

    @property
    def __str__(self):
        self_dict = dict()

        if self.type == "point-transform":
            self_dict["type"] = "point-transform"
            self_dict["a"] = str(self.a)

        if self.type == "line-transform":
            self_dict["type"] = "line-transform"
            self_dict["L"] = str(self.L)

        if self.type == "value-transform":
            self_dict["type"] = "value-transform"
            self_dict["values"] = str(self.affine)
            self_dict["is_affine"] = str(self.is_affine)
        return str(self_dict)

    @property
    def is_affine(self):
        return self.affine[0] + self.affine[1] == 1

    @staticmethod
    def rot_B_in_A(A, B):
        """ page 52 """
        return
        # return GmodPointTransform(A, B, (Gmod(2), Gmod(28)))

    @staticmethod
    def reflect_point_in_line(line, point):
        """ page 52 """
        assert not line.is_null_line()
        a = line.a
        b = line.b
        c = line.c
        x = point.x
        y = point.y
        new_x = ((b ** 2 - a ** 2) * x - a * b * y * 2 - 2 * a * c) / (a ** 2 + b ** 2)
        new_y = (-a * b * x * 2 + (a ** 2 - b ** 2) *
                 y - b * c * 2) / (a ** 2 + b ** 2)
        return GmodA2Point(new_x, new_y)

    @staticmethod
    def rot_line_in_point(point, line):
        a = line.a
        b = line.b
        c = line.c
        x = point.x
        y = point.y
        return GmodA2Line(a, b, -x * a * 2 - y * b * 2 - c)

    @staticmethod
    def reflect_line_in_line(line1, line2):
        assert not line1.is_null_line()
        """ page 54 """
        a = line1.a
        b = line1.b
        c = line1.c
        a1 = line2.a
        b1 = line2.b
        c1 = line2.c

        new_a = (a ** 2 - b ** 2) * a1 + a * b * b1 * 2
        new_b = a * b * a1 * 2 - (a ** 2 - b ** 2) * b1
        new_c = a * c * a1 * 2 + b * c * b1 * 2 - (a ** 2 + b ** 2) * c1
        return GmodA2Line(new_a, new_b, new_c)


GmodA2Transform.ALL_AFFINE_TRANSFORMS = [GmodA2Transform([gmod, -gmod + 1]) for gmod in Gmod.ALL_GMODS]


class GmodA2Subplot:

    def __init__(self, ax, xlabel="x", ylabel="y"):
        self.ax = ax
        GmodA2Subplot.add_2d_gridlines(self.ax, xlabel, ylabel)
        self.plotted_geoms = list()

    def plot(self, geom_object, style=False):
        if not style:
            style = "ko"

        if isinstance(geom_object, GmodA2GeometryObject):
            geom_object.__plot_on_xy__(self.ax, style)
            self.plotted_geoms.append(geom_object)
            # print(geom_object)
        elif isinstance(geom_object, list):
            for geom_obj in geom_object:
                self.plot(geom_obj, style)
        else:
            raise ValueError("Unknown type in plot " + str(type(geom_object)))

    @staticmethod
    def add_2d_gridlines(ax, xlabel, ylabel):
        ax.set_xlim([0, 29])
        ax.set_ylim([0, 29])
        ax.set_xlabel(xlabel)
        ax.set_ylabel(ylabel)
        # Don't allow the axis to be on top of your data
        ax.set_axisbelow(True)

        # Turn on the minor TICKS, which are required for the minor GRID
        ax.minorticks_on()

        # Major ticks every 20, minor ticks every 5
        major_ticks = np.arange(0, 29, 5)
        minor_ticks = np.arange(0, 29, 1)

        ax.set_xticks(major_ticks)
        ax.set_xticks(minor_ticks, minor=True)
        ax.set_yticks(major_ticks)
        ax.set_yticks(minor_ticks, minor=True)

        # Customize the major grid
        ax.grid(which='major', linestyle='-', linewidth='0.5', color='red')
        # Customize the minor grid
        ax.grid(which='minor', linestyle=':', linewidth='0.5', color='black')
        # Turn off the display of all ticks.
        # ax.tick_params(which='both',  # Options for both major and minor ticks
        #              top=False,  # turn off top ticks
        #             left=False,  # turn off left ticks
        #            right=False,  # turn off right ticks
        #           bottom=False)  # turn off bottom ticks


class GmodA2Figure:
    FIGURE_COUNT = 1
    COLORCODES = ["b", "g", "c", "r", "m", "y", "k"]

    plotted_geoms = list()

    def __init__(self, title=None):
        self.figure = plt.figure(GmodA2Figure.FIGURE_COUNT, figsize=(11, 8))
        GmodA2Figure.FIGURE_COUNT += 1

        gs = self.figure.add_gridspec(1, 1)
        self.ax_xy = GmodA2Subplot(self.figure.add_subplot(gs[0, 0], adjustable='box', aspect=1.0))

        if title is not None:
            self.figure.suptitle(title)

        self.figure.tight_layout()

    def plot(self, geom_object, style=False):
        if not style:
            style = "ro"

        if isinstance(geom_object, GmodA2GeometryObject):
            self.ax_xy.plot(geom_object, style)
            print(geom_object)
            print_separator()
        elif isinstance(geom_object, list):
            for geom in geom_object:
                self.plot(geom, style)
        else:
            assert isinstance(geom_object, GmodA2GeometryObject)


"""
WANT_TO_KNOW = list()

KNOWN_LINES = get_known_lines()

UNIT_CIRCLE_KNOWN_INTERSECTS = (get_intersects_known_lines(GmodCircle.UNIT_CIRCLE), "bo")
PROJ_CIRCLE_KNOWN_INTERSECTS = (get_intersects_known_lines(GmodCircle.PROJECTIVE_CIRCLE), "go")

WANT_TO_KNOW.append(UNIT_CIRCLE_KNOWN_INTERSECTS)
# WANT_TO_KNOW.append(PROJ_CIRCLE_KNOWN_INTERSECTS)

INT_UNIT_QUAD1_1 = (GmodQuad(GmodPoint(11,24), GmodPoint(24,11), GmodPoint(16,8), GmodPoint(8,16)), "go")
INT_UNIT_QUAD1_2 = (GmodQuad(GmodPoint(18,24), GmodPoint(24,18), GmodPoint(21,16), GmodPoint(16,21)), "bo" )

# WANT_TO_KNOW.append(INT_UNIT_QUAD1)
# WANT_TO_KNOW.append(INT_UNIT_QUAD2)

UNIT_HEXAHEDRON = (GmodHexahedron(INT_UNIT_QUAD1_1[0], INT_UNIT_QUAD1_2[0]), "bo")
# WANT_TO_KNOW.append(UNIT_HEXAHEDRON)

INT_UNIT_QUAD2_1 = GmodQuad(GmodPoint(18, 24), GmodPoint(24, 18), GmodPoint(16, 21), GmodPoint(21, 16))
INT_UNIT_QUAD2_2 = GmodQuad(GmodPoint(11, 24), GmodPoint(24, 11), GmodPoint(8, 16), GmodPoint(16, 8))
UNIT_HEX2 = (GmodHexahedron(INT_UNIT_QUAD2_1, INT_UNIT_QUAD2_2), "bo")

# WANT_TO_KNOW.append(UNIT_HEX2)

P1_1 = GmodPoint(18, 24)
P1_2 = GmodPoint(24, 18)
P1_3 = GmodPoint(16, 21)
P1_4 = GmodPoint(21, 16)

P2_1 = GmodPoint(11, 24)
P2_2 = GmodPoint(24, 11)
P2_3 = GmodPoint(8, 16)
P2_4 = GmodPoint(16, 8)

INT_UNIT_QUAD3_1 = GmodQuad(P1_1, P1_3, P1_2, P1_4)
INT_UNIT_QUAD3_2 = GmodQuad(P2_4, P2_2, P2_3, P2_1)
UNIT_HEX3 = (GmodHexahedron(INT_UNIT_QUAD3_1, INT_UNIT_QUAD3_2), "bo")

#WANT_TO_KNOW.append(UNIT_HEX3)
"""
