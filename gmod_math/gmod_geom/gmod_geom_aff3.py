from .gmod_geom_aff2 import *


class GmodA3GeometryObject(GmodGeometryObject):

    @property
    def to_gmod_matrix(self):
        raise NotImplementedError("Must override to_gmod_matrix")

    def is_a3_incident_with_point(self, point):
        assert isinstance(point, GmodA3Point)
        return point in self.to_gmod_matrix

    def a3_intersects_a3(self, other):
        assert isinstance(other, GmodA3GeometryObject)
        return [point for point in self.to_gmod_matrix if point in other.to_gmod_matrix]

    def __plot_on_3d__(self, ax, style):
        raise NotImplementedError("Must override __plot_on_3d__")

    def __plot_on_xy__(self, ax, style):
        raise NotImplementedError("Must override __plot_on_xy__")

    def __plot_on_yz__(self, ax, style):
        raise NotImplementedError("Must override __plot_on_yz__")

    def __plot_on_xz__(self, ax, style):
        raise NotImplementedError("Must override __plot_on_xz__")


class GmodA3Point(GmodPoint, GmodA3GeometryObject):
    ORIGIN_3D = None

    def __init__(self, *args):
        super().__init__(*args)
        assert len(self) == 3

    @property
    def x(self):
        return Gmod(self[0])

    @property
    def y(self):
        return Gmod(self[1])

    @property
    def z(self):
        return Gmod(self[2])

    @property
    def cartisian(self):
        return GmodMatrix.h_mat(self)

    @property
    def point_vector(self):
        return GmodA3Vector(self.x, self.y, self.z)

    @property
    def point_permutations(self):
        return [GmodA3Point(perm[0], perm[1], perm[2]) for perm in
                list(itertools.permutations([self.x, self.y, self.z]))]

    @property
    def to_gmod_matrix(self):
        return [self]

    def parallel_proj_a2_point(self, index):
        if index == 0:
            return GmodA2Point(self[1], self[2])
        elif index == 1:
            return GmodA2Point(self[0], self[2])
        elif index == 2:
            return GmodA2Point(self[0], self[1])
        else:
            raise ValueError("Invalid index")

    def vector_dif(self, other):
        assert isinstance(other, GmodA3Point)
        return GmodA3Vector(other.x - self.x, other.y - self.y, other.z - self.z)

    def __plot_on_3d__(self, ax, style):
        x = [int(self.x)]
        y = [int(self.y)]
        z = [int(self.z)]

        mplpoint = ax.plot(x, y, z, style, label="x:{}\ny:{}\nz:{}".format(x, y, z))
        # datacursor(formatter='{label}'.format)
        # mplcursors.cursor(mplpoint)
        # xyz = (x, y, z)  # <--
        # ax.annotate('(%s, %s, %s)' % xyz, xyz=xyz, textcoords='data')

    def __plot_on_xy__(self, ax, style):
        ax.plot(self.parallel_proj_a2_point(2), style)

    def __plot_on_yz__(self, ax, style):
        ax.plot(self.parallel_proj_a2_point(0), style)

    def __plot_on_xz__(self, ax, style):
        ax.plot(self.parallel_proj_a2_point(1), style)

    @staticmethod
    def quadrance_between_points(point1, point2):
        assert isinstance(point1, GmodA3Point) and isinstance(point2, GmodA3Point)
        return GmodA3Vector.quadrance_between_vectors(point1.point_vector, point2.point_vector)

    @staticmethod
    def points_are_colinear(point1, point2, point3):
        """ page 39 """
        assert isinstance(point1, GmodA3Point) and isinstance(point2, GmodA3Point) and isinstance(point3, GmodA3Point)

        coline = GmodA3Line.make_line(point1, point2)
        return GmodA3Line.line_incident_with_point(coline, point3)

    @staticmethod
    def colinear_line_for_points(point1, point2, point3):
        """ page 39 """
        assert isinstance(point1, GmodA3Point) and isinstance(point2, GmodA3Point) and isinstance(point3, GmodA3Point)
        assert GmodA3Point.points_are_colinear(point1, point2, point3)
        coline = GmodA3Line.make_line(point1, point2)

        assert GmodA3Line.line_incident_with_point(coline, point1) and \
               GmodA3Line.line_incident_with_point(coline, point2) and \
               GmodA3Line.line_incident_with_point(coline, point3)

        return coline


ORIGIN_3D = GmodA3Point(0, 0, 0)


class GmodA3Vector(GmodProportionStruct):
    """ used to be called side"""

    def __init__(self, x, y, z):
        super().__init__([x, y, z])

    def parallel_proj_a2_vec(self, index):
        if index == 0:
            return GmodA2Vector(self[1], self[2])
        elif index == 1:
            return GmodA2Vector(self[0], self[2])
        elif index == 2:
            return GmodA2Vector(self[0], self[1])
        else:
            raise ValueError("Invalid index")

    @property
    def x(self):
        return Gmod(self[0])

    @property
    def y(self):
        return Gmod(self[1])

    @property
    def z(self):
        return Gmod(self[2])

    @property
    def vector_quadrance(self):
        return self.x ** 2 + self.y ** 2 + self.z ** 2

    @property
    def vector_point(self):
        return GmodA3Point(self.x, self.y, self.z)

    @property
    def cartisian(self):
        return self.h_mat

    @property
    def vector_permutations(self):
        return [GmodA3Vector(perm[0], perm[1], perm[2]) for perm in
                list(itertools.permutations([self.x, self.y, self.z]))]

    @staticmethod
    def solid_spread(vec1, vec2, vec3):
        assert isinstance(vec1, GmodA3Vector) and isinstance(vec2, GmodA3Vector) and isinstance(vec3, GmodA3Vector)
        return GmodMatrix([vec1, vec2, vec3]).det() ** 2 // (vec1.quadr * vec2.quadr * vec3.quadr)

    @staticmethod
    def quadrance_between_vectors(vec1, vec2):
        assert isinstance(vec1, GmodA3Vector) and isinstance(vec2, GmodA3Vector)
        quadrance = Gmod(0)
        for x in range(len(vec1)):
            quadrance = quadrance + (vec2[x] - vec1[x]) ** 2

        return quadrance

    @staticmethod
    def spread_between_vectors(vec1, vec2):
        assert isinstance(vec1, GmodA3Vector) and isinstance(vec2, GmodA3Vector)
        Q1 = vec1.vector_quadrance
        Q2 = vec2.vector_quadrance
        return Gmod(1) - (vec1.dot(vec2)) ** 2 // (Q1 * Q2)

    @staticmethod
    def vectors_are_para(vec1, vec2):
        assert isinstance(vec1, GmodA3Vector) and isinstance(vec2, GmodA3Vector)
        x1 = vec1.x
        x2 = vec2.x
        y1 = vec1.y
        y2 = vec2.y
        z1 = vec1.z
        z2 = vec2.z
        return x1 * y2 - x2 * y1 == y1 * z2 - y2 * z1 == z1 * x2 - z2 * x1 == 0

    @staticmethod
    def vectors_are_perp(vec1, vec2):
        assert isinstance(vec1, GmodA3Vector) and isinstance(vec2, GmodA3Vector)
        vec_dif = vec2 - vec1
        return vec1.vector_quadrance + vec2.vector_quadrance == vec_dif.quadrance

    @property
    def __str__(self):
        return self.cartisian.__strrepr__


class GmodA3Line(GmodA3GeometryObject):

    def __init__(self, point, vector):
        assert isinstance(point, GmodA3Point) and isinstance(vector, GmodA3Vector)
        self.base_point = point
        self.direction_vector = vector

    @property
    def to_gmod_matrix(self):
        all_inc_points = list()
        for gmod in Gmod.ALL_GMODS:
            all_inc_points.append(self(gmod))
        return all_inc_points

    @property
    def __printdict__(self):
        self_dict = dict()

        self_dict["line_base"] = self.base_point.__strrepr__
        self_dict["line_direction"] = self.direction_vector.__strrepr__

        return self_dict

    def __call__(self, value):
        value = Gmod(value)
        return self.base_point + (self.direction_vector * value).transpose()

    def __eq__(self, other):
        assert isinstance(other, GmodA3Line)
        for point in self.to_gmod_matrix:
            if not point in other.to_gmod_matrix:
                return False
        return True

    def __plot_on_3d__(self, ax, style):
        for point in self.to_gmod_matrix:
            point.__plot_on_3d__(ax, style)

    def __plot_on_xy__(self, ax, style):
        for point in self.to_gmod_matrix:
            ax.plot(point.make_a2_point(2), style)

    def __plot_on_yz__(self, ax, style):
        for point in self.to_gmod_matrix:
            ax.plot(point.make_a2_point(0), style)

    def __plot_on_xz__(self, ax, style):
        for point in self.to_gmod_matrix:
            ax.plot(point.make_a2_point(1), style)

    @staticmethod
    def lines_are_para(line1, line2):
        assert isinstance(line1, GmodA3Line) and isinstance(line2, GmodA3Line)
        return GmodA3Vector.vectors_are_para(line1.direction_vector, line2.direction_vector)

    @staticmethod
    def lines_are_perp(line1, line2):
        assert isinstance(line1, GmodA3Line) and isinstance(line2, GmodA3Line)
        return GmodA3Vector.vectors_are_perp(line1.direction_vector, line2.direction_vector)

    @staticmethod
    def line_incident_with_point(line, point):
        assert isinstance(line, GmodA3Line) and isinstance(point, GmodA3Point)
        return point in line.to_gmod_matrix

    @staticmethod
    def meet_of_lines(line1, line2):
        assert isinstance(line1, GmodA3Line) and isinstance(line1, GmodA3Line)
        for point in line1.to_gmod_matrix:
            if point in line2.to_gmod_matrix:
                return point

        return None

    @staticmethod
    def make_line(point1, point2):
        vec_dif = point2.vector_dif(point1)
        new_line = GmodA3Line(point1, vec_dif)
        assert GmodA3Line.line_incident_with_point(new_line, point1) and GmodA3Line.line_incident_with_point(new_line,
                                                                                                             point2)
        return new_line

    @staticmethod
    def spread_between_lines(line1, line2):
        assert isinstance(line1, GmodA3Line) and isinstance(line1, GmodA3Line)
        return GmodA3Vector.spread_between_vectors(line1.direction_vector, line2.direction_vector)

    @staticmethod
    def lines_are_concurrent(line1, line2, line3):
        """ page 39 """
        assert isinstance(line1, GmodA3Line) and isinstance(line2, GmodA3Line) and isinstance(line3, GmodA3Line)
        for point in line1.to_gmod_matrix:
            if point in line2.to_gmod_matrix and point in line3.to_gmod_matrix:
                return True

        return False

    @staticmethod
    def concurrent_point_of_lines(line1, line2, line3):
        """ page 39 """
        assert isinstance(line1, GmodA2Line) and isinstance(line2, GmodA2Line) and isinstance(line3, GmodA2Line)
        copoint = GmodA3Line.meet_of_lines(line1, line2)
        assert GmodA3Line.line_incident_with_point(line3, copoint)
        return copoint


class GmodA3Side(GmodA3GeometryObject):
    """ used to be called side"""

    def __init__(self, A1, A2):
        self.A1 = A1
        self.A2 = A2

    @property
    def side_points(self):
        return [self.A1, self.A2]

    @property
    def side_line(self):
        return GmodA3Line.make_line(self.A1, self.A2)

    @property
    def side_vector(self):
        return self.A1.vector_dif(self.A2)

    @property
    def side_quadrance(self):
        return GmodA3Point.quadrance_between_points(self.A1, self.A2)

    @property
    def side_midpoint(self):
        return (self.A1 + self.A2) * Gmod(1 / 2)

    def __plot_on_xy__(self, ax, style):
        aff1 = self.A1.parallel_proj_a2_point(2)
        aff2 = self.A2.parallel_proj_a2_point(2)
        ax.plot(GmodA2Side(aff1, aff2), style)

    def __plot_on_yz__(self, ax, style):
        aff1 = self.A1.parallel_proj_a2_point(0)
        aff2 = self.A2.parallel_proj_a2_point(0)
        ax.plot(GmodA2Side(aff1, aff2), style)

    def __plot_on_xz__(self, ax, style):
        aff1 = self.A1.parallel_proj_a2_point(1)
        aff2 = self.A2.parallel_proj_a2_point(1)
        ax.plot(GmodA2Side(aff1, aff2), style)

    def __plot_on_3d__(self, ax, style):
        xs = [int(self.A1.x), int(self.A2.x)]
        ys = [int(self.A1.y), int(self.A2.y)]
        zs = [int(self.A1.z), int(self.A2.z)]

        q = int(self.side_quadrance)
        mplside = ax.plot(xs, ys, zs, "-" + str(style), label="Q:{}".format(q))
        # ax.annotate('(%s, %s, %s)' % xyz, xyz=xyz, textcoords='data')

    @property
    def to_gmod_matrix(self):
        return [self.A1, self.A2]

    @property
    def __printdict__(self):
        self_dict = dict()
        self_dict["side_points"] = [point.__strrepr__ for point in self.side_points]
        self_dict["Q"] = self.side_quadrance.__strrepr__
        self_dict["midpoint"] = self.side_midpoint.__strrepr__
        return self_dict

    @staticmethod
    def sides_are_para(side1, side2):
        assert isinstance(side1, GmodA3Side) and isinstance(side2, GmodA3Side)
        return GmodA3Vector.vectors_are_para(side1.side_vector, side2.side_vector)

    @staticmethod
    def sides_are_perp(side1, side2):
        assert isinstance(side1, GmodA3Side) and isinstance(side2, GmodA3Side)
        return GmodA3Vector.vectors_are_perp(side1.side_vector, side2.side_vector)


class GmodA3Vertex:

    def __init__(self, line1, line2):
        assert isinstance(line1, GmodA3Line) and isinstance(line2, GmodA3Line)
        # assert self.line1 != self.line2
        self.line1 = line1
        self.line2 = line2

    @property
    def vertex_lines(self):
        return [self.line1, self.line2]

    @property
    def vertex_point(self):
        return GmodA3Line.meet_of_lines(self.vertex_lines[0], self.vertex_lines[1])

    @property
    def vertex_spread(self):
        return GmodA3Line.spread_between_lines(self.vertex_lines[0], self.vertex_lines[1])

    @property
    def __printdict__(self):
        self_dict = dict()
        self_dict["vertex_lines"] = [line.__printdict__ for line in self.vertex_lines]
        self_dict["point"] = self.vertex_point.__strrepr__
        self_dict["s"] = self.vertex_spread.__strrepr__

        return self_dict


class GmodA3Plane(GmodMatrix, GmodA3GeometryObject):

    def __new__(cls, a, b, c, d):
        return super().__init__([a, b, c, d])

    @property
    def a(self):
        return Gmod(self[0])

    @property
    def b(self):
        return Gmod(self[1])

    @property
    def c(self):
        return Gmod(self[2])

    @property
    def d(self):
        return Gmod(self[3])

    @property
    def cartisian_mat(self):
        mat1 = GmodMatrix([self.a, self.b, self.c])
        mat2 = GmodMatrix([self.d])
        return [mat1, mat2]

    @property
    def to_gmod_matrix(self):
        inc_points = list()

        para_point1 = self(0, 0)
        para_point2 = self(1, 0)
        para_point3 = self(2, 1)

        para_vec1 = para_point1.vector_dif(para_point2)
        para_vec2 = para_point1.vector_dif(para_point3)

        for x in Gmod.ALL_GMODS:
            for y in Gmod.ALL_GMODS:
                para_eval = para_point1.cartisian_mat + \
                            (para_vec1.cartisian * x).transpose() + \
                            (para_vec2.cartisian * y).transpose()

                inc_points.append(GmodA3Point(para_eval[0][0], para_eval[0][1], para_eval[0][2]))

        return inc_points

    def __call__(self, *args):
        if len(args) == 2:  # have x and y find z
            x = Gmod(args[0])
            y = Gmod(args[1])
            z = (self.d - self.a * x - self.b * y) // self.c
            return GmodA3Point(x, y, z)
        else:
            assert len(args) == 2

    def __plot_on_3d__(self, ax, style):
        pass

    def __plot_on_xy__(self, ax, style):
        pass

    def __plot_on_yz__(self, ax, style):
        pass

    def __plot_on_xz__(self, ax, style):
        pass

    @staticmethod
    def point_meet_of_three_planes(plane1, plane2, plane3):
        left = GmodMatrix([[plane1.a, plane1.b, plane1.c],
                           [plane2.a, plane2.b, plane2.c],
                           [plane3.a, plane3.b, plane3.c]])
        right = GmodMatrix([[plane1.d],
                            [plane2.d],
                            [plane3.d]])
        meet_eval = left.solve(right)
        return GmodA3Point(meet_eval[0][0], meet_eval[1][0], meet_eval[2][0])


class GmodA3Ngon(GmodA3GeometryObject):

    def __init__(self, *args):
        assert len(args) >= 3

        self.ngon_A_n = list()
        for arg in args:
            assert isinstance(arg, GmodA3Point)
            self.ngon_A_n.append(arg)

    @property
    def __printdict__(self):
        print_dict = dict()

        print_dict["points"] = [str(point) for point in self.ngon_A_n]
        print_dict["Q"] = [str(quadr) for quadr in self.ngon_quadrs]
        print_dict["s"] = [str(spread) for spread in self.ngon_spreads]
        print_dict["lines"] = [line.__printdict__ for line in self.ngon_lines]

        # print_dict["sides"] = [side.triangle_info for side in self.sides]
        # print_dict["vertices"] = [vert.triangle_info for vert in self.vertices]

        return print_dict

    @property
    def ngon_sides(self):
        length = len(self.ngon_A_n)
        ngon_sides = list()
        for x in range(length):
            curr_A = self.ngon_A_n[x % length]
            next_A = self.ngon_A_n[(x + 1) % length]
            curr_side = GmodA3Side(curr_A, next_A)
            ngon_sides.append(curr_side)
        return ngon_sides

    @property
    def ngon_quadrs(self):
        return [side.side_quadrance for side in self.ngon_sides]

    @property
    def ngon_lines(self):
        return [side.side_line for side in self.ngon_sides]

    @property
    def ngon_verts(self):
        length = len(self.ngon_lines)
        ngon_verts = list()
        for x in range(length):
            curr_L = self.ngon_lines[(x + 1) % length]
            next_L = self.ngon_lines[(x + 2) % length]
            curr_vert = GmodA3Vertex(curr_L, next_L)
            ngon_verts.append(curr_vert)
        return ngon_verts

    @property
    def ngon_spreads(self):
        return [vert.vertex_spread for vert in self.ngon_verts]

    @property
    def ngon_crosses(self):
        return [Gmod(1) - spread for spread in self.ngon_spreads]

    @property
    def ngon_midpoints(self):
        return [side.side_midpoint for side in self.ngon_sides]

    @property
    def to_gmod_matrix(self):
        inc_points = list()
        for side in self.ngon_sides:
            for side_point in side.to_gmod_matrix:
                inc_points.append(side_point)
        return inc_points

    def __plot_on_3d__(self, ax, style):
        for n_A in self.ngon_A_n:
            n_A.__plot_on_3d__(ax, style)
        for n_side in self.ngon_sides:
            n_side.__plot_on_3d__(ax, style)

    def __plot_on_xy__(self, ax, style):
        for n_A in self.ngon_A_n:
            n_A.__plot_on_xy__(ax, style)
        for n_side in self.ngon_sides:
            n_side.__plot_on_xy__(ax, style)

    def __plot_on_yz__(self, ax, style):
        for n_A in self.ngon_A_n:
            n_A.__plot_on_yz__(ax, style)
        for n_side in self.ngon_sides:
            n_side.__plot_on_yz__(ax, style)

    def __plot_on_xz__(self, ax, style):
        for n_A in self.ngon_A_n:
            n_A.__plot_on_xz__(ax, style)
        for n_side in self.ngon_sides:
            n_side.__plot_on_xz__(ax, style)


class GmodA3Triangle(GmodA3Ngon):

    def __init__(self, A, B, C):
        assert isinstance(A, GmodA3Point) and \
               isinstance(B, GmodA3Point) and \
               isinstance(C, GmodA3Point)

        super().__init__(A, B, C)

    def thales_theorem(self, value):
        assert value != 0
        B2 = self.A * (-value + 1) + self.B * value
        B3 = self.A * (-value + 1) + self.C * value

        assert GmodA3Side.sides_are_para(self.BC, GmodA2Side(B2, B3))
        return GmodA2Triangle(self.A, B2, B3)

    @property
    def A(self):
        return self.ngon_A_n[0]

    @property
    def B(self):
        return self.ngon_A_n[1]

    @property
    def C(self):
        return self.ngon_A_n[2]

    @property
    def AB(self):
        return self.ngon_sides[0]

    @property
    def BC(self):
        return self.ngon_sides[1]

    @property
    def CA(self):
        return self.ngon_sides[2]

    @property
    def triple_quad_colinear(self):
        Q1 = self.ngon_quadrs[0]
        Q2 = self.ngon_quadrs[1]
        Q3 = self.ngon_quadrs[2]
        return (Q1 + Q2 + Q3) ** 2 == (Q1 ** 2 + Q2 ** 2 + Q3 ** 2) * 2

    @property
    def pyth_theorem_perp(self):
        Q1 = self.ngon_quadrs[0]
        Q2 = self.ngon_quadrs[1]
        Q3 = self.ngon_quadrs[2]
        return Q1 + Q2 == Q3

    @property
    def spread_law(self):
        Q1 = self.ngon_quadrs[0]
        Q2 = self.ngon_quadrs[1]
        Q3 = self.ngon_quadrs[2]
        s1 = self.ngon_spreads[0]
        s2 = self.ngon_spreads[1]
        s3 = self.ngon_spreads[2]
        spread_law_eval = s1 // Q1 == s2 // Q2 == s3 // Q3
        if spread_law_eval:
            return True
        else:
            return str(s1) + "//" + str(Q1) + "!=" \
                   + str(s2) + "//" + str(Q2) + "!=" \
                   + str(s3) + "//" + str(Q3)

    @property
    def cross_law(self):
        Q1 = self.ngon_quadrs[0]
        Q2 = self.ngon_quadrs[1]
        Q3 = self.ngon_quadrs[2]
        c3 = self.ngon_crosses[2]
        return (Q1 + Q2 - Q3) ** 2 == Q1 * Q2 * c3 * 4

    @property
    def triple_spread_law(self):
        s1 = self.ngon_spreads[0]
        s2 = self.ngon_spreads[1]
        s3 = self.ngon_spreads[2]
        return (s1 + s2 + s3) ** 2 == (s1 ** 2 + s2 ** 2 + s3 ** 2) * 2 + s1 * s2 * s3 * 4

    @property
    def is_logistic_map(self):
        l1 = self.ngon_sides[0].side_line
        l2 = self.ngon_sides[1].side_line
        l3 = self.ngon_sides[2].side_line

        s = GmodA3Line.spread_between_lines(l1, l2)
        r = GmodA3Line.spread_between_lines(l2, l3)
        if s == r:
            if r == 0:
                return False
            else:
                return (s * 4) * (-s + 1)
        return False

    @property
    def triangle_medians(self):
        assert len(self.ngon_A_n) == len(self.ngon_sides)
        length = len(self.ngon_A_n)
        return [GmodA3Side(self.ngon_A_n[x], self.ngon_sides[(x + 1) % length].side_midpoint)
                for x in range(len(self.ngon_A_n))]

    @property
    def triangle_centroid(self):
        cent1 = GmodA3Line.meet_of_lines(self.triangle_medians[0].side_line, self.triangle_medians[1].side_line)
        cent2 = GmodA3Line.meet_of_lines(self.triangle_medians[1].side_line, self.triangle_medians[2].side_line)
        cent3 = GmodA3Line.meet_of_lines(self.triangle_medians[2].side_line, self.triangle_medians[0].side_line)
        assert cent1 == cent2 == cent3
        return cent1

    # noinspection PyTypeChecker
    @property
    def __printdict__(self):
        tri_dict = super().__printdict__

        tri_dict["medians"] = [median.__printdict__ for median in self.triangle_medians]
        tri_dict["centroid"] = self.triangle_centroid.__strrepr__

        tri_dict["TripleQuadColinear"] = str(self.triple_quad_colinear)
        tri_dict["PythTheoremPerp"] = str(self.pyth_theorem_perp)
        tri_dict["SpreadLaw"] = str(self.spread_law)
        tri_dict["CrossLaw"] = str(self.cross_law)
        tri_dict["TripleSpreadLaw"] = str(self.triple_spread_law)
        tri_dict["IsLogisticMap"] = str(self.is_logistic_map)
        return tri_dict

    @staticmethod
    def calc_spread_cross_law(q1, q2, q3):
        numer = (q1 + q2 - q3) ** 2
        denom = q1 * q2 * Gmod(4)
        spread = Gmod(1) - (numer // denom)
        return spread


class GmodA3Quad(GmodA3Ngon, GmodA3GeometryObject):
    """
        denotes a rectangle in the ffe with:
            point of bottom left of rect ["bot-l"]
            point of top right of rect ["top-r"]

       can accept GmodPoints
    """

    def __init__(self, A1, A2, A3, A4):
        super().__init__(A1, A2, A3, A4)

    @property
    def quad_diags(self):
        diag_AC = GmodA3Side(self.ngon_A_n[0], self.ngon_A_n[2])
        diag_BD = GmodA3Side(self.ngon_A_n[1], self.ngon_A_n[3])
        return [diag_AC, diag_BD]

    @property
    def quad_bisectors(self):
        bisector1 = GmodA3Side(self.ngon_sides[0].side_midpoint, self.ngon_sides[2].side_midpoint)
        bisector2 = GmodA3Side(self.ngon_sides[1].side_midpoint, self.ngon_sides[3].side_midpoint)
        return [bisector1, bisector2]

    @property
    def is_parallelogram(self):
        return GmodA3Side.sides_are_para(self.ngon_sides[0], self.ngon_sides[2]) and \
               GmodA3Side.sides_are_para(self.ngon_sides[1], self.ngon_sides[3])

    @property
    def is_rectangle(self):
        return self.is_parallelogram and \
               GmodA3Side.sides_are_perp(self.quad_bisectors[0], self.quad_bisectors[1])

    @property
    def is_rhombus(self):
        return self.is_parallelogram and \
               GmodA3Side.sides_are_perp(self.quad_diags[0], self.quad_diags[1])

    @property
    def is_square(self):
        return self.is_rectangle and self.is_rhombus

    @property
    def quad_center(self):
        # assert self.is_parallelogram
        return GmodA3Line.meet_of_lines(self.quad_diags[0].side_line, self.quad_diags[1].side_line)

    @property
    def midpoint_quad(self):
        """ page 49 (share the same center) """
        return GmodA2Quad(self.ngon_sides[0].side_midpoint,
                          self.ngon_sides[1].side_midpoint,
                          self.ngon_sides[2].side_midpoint,
                          self.ngon_sides[3].side_midpoint)

    @property
    def __printdict__(self):
        quad_dict = super().__printdict__
        # quad_dict["diags"] = [diag.triangle_info for diag in self.quad_diags]
        quad_dict["quad_center"] = str(self.quad_center)
        quad_dict["IsParallelogram"] = str(self.is_parallelogram)
        quad_dict["IsRectangle"] = str(self.is_rectangle)
        quad_dict["IsRhombus"] = str(self.is_rhombus)
        return quad_dict


class GmodA3PlatonicSolid:
    @property
    def VERTEX_COUNT(self):
        raise NotImplementedError()

    @property
    def EDGE_COUNT(self):
        raise NotImplementedError()

    @property
    def FACE_COUNT(self):
        raise NotImplementedError()

    @property
    def dual_platonic_solid(self):
        raise NotImplementedError()


class GmodA3Tetrahedron(GmodA3GeometryObject, GmodA3PlatonicSolid):

    @property
    def VERTEX_COUNT(self):
        return 4

    @property
    def EDGE_COUNT(self):
        return 6

    @property
    def FACE_COUNT(self):
        return 4

    def __init__(self, *args):
        assert len(args) == self.VERTEX_COUNT
        for point in args:
            assert isinstance(point, GmodA3Point)
        self.tetrahedron_points = args

    @property
    def tetrahedron_sides(self):
        tetra_sides = list()
        for x in range(len(self.tetrahedron_points)):
            for y in range(x + 1, len(self.tetrahedron_points)):
                newside = GmodA3Side(self.tetrahedron_points[x], self.tetrahedron_points[y])
                tetra_sides.append(newside)
        return tetra_sides

    @property
    def tetrahedron_side_quadrs(self):
        return [side.side_quadrance for side in self.tetrahedron_sides]

    @property
    def tetrahedron_faces(self):
        return [GmodA3Triangle(self.tetrahedron_points[0], self.tetrahedron_points[1], self.tetrahedron_points[2]),
                GmodA3Triangle(self.tetrahedron_points[1], self.tetrahedron_points[2], self.tetrahedron_points[3]),
                GmodA3Triangle(self.tetrahedron_points[0], self.tetrahedron_points[2], self.tetrahedron_points[3]),
                GmodA3Triangle(self.tetrahedron_points[0], self.tetrahedron_points[1], self.tetrahedron_points[3])]

    @property
    def dual_platonic_solid(self):
        return GmodA3Tetrahedron(*[side.side_midpoint for side in self.tetrahedron_sides])

    @property
    def to_gmod_matrix(self):
        return self.tetrahedron_points

    def __plot_on_3d__(self, ax, style):
        for side in self.tetrahedron_sides:
            side.__plot_on_3d__(ax, style)

    def __plot_on_xy__(self, ax, style):
        for side in self.tetrahedron_sides:
            side.__plot_on_xy__(ax, style)

    def __plot_on_yz__(self, ax, style):
        for side in self.tetrahedron_sides:
            side.__plot_on_yz__(ax, style)

    def __plot_on_xz__(self, ax, style):
        for side in self.tetrahedron_sides:
            side.__plot_on_xz__(ax, style)

    @property
    def __printdict__(self):
        self_dict = dict()

        self_dict["points"] = [point.__strrepr__ for point in self.tetrahedron_points]
        self_dict["Q"] = [quadr.__strrepr__ for quadr in self.tetrahedron_side_quadrs]

        return self_dict


class GmodA3Cube(GmodA3GeometryObject, GmodA3PlatonicSolid):

    @property
    def VERTEX_COUNT(self):
        return 8

    @property
    def EDGE_COUNT(self):
        return 12

    @property
    def FACE_COUNT(self):
        return 6

    def __init__(self, *args):
        assert len(args) == self.VERTEX_COUNT
        for point in args:
            assert isinstance(point, GmodA3Point)

        self.cube_points = args

    @property
    def cube_front(self):
        return GmodA3Quad(self.cube_points[0],
                          self.cube_points[1],
                          self.cube_points[2],
                          self.cube_points[3])

    @property
    def cube_back(self):
        return GmodA3Quad(self.cube_points[4],
                          self.cube_points[5],
                          self.cube_points[6],
                          self.cube_points[7])

    @property
    def cube_top(self):
        return GmodA3Quad(self.cube_front.ngon_A_n[0],
                          self.cube_front.ngon_A_n[1],
                          self.cube_back.ngon_A_n[1],
                          self.cube_back.ngon_A_n[0])

    @property
    def cube_right(self):
        return GmodA3Quad(self.cube_front.ngon_A_n[1],
                          self.cube_front.ngon_A_n[2],
                          self.cube_back.ngon_A_n[2],
                          self.cube_back.ngon_A_n[1])

    @property
    def cube_bottom(self):
        return GmodA3Quad(self.cube_front.ngon_A_n[2],
                          self.cube_front.ngon_A_n[3],
                          self.cube_back.ngon_A_n[3],
                          self.cube_back.ngon_A_n[2])

    @property
    def cube_left(self):
        return GmodA3Quad(self.cube_front.ngon_A_n[3],
                          self.cube_front.ngon_A_n[0],
                          self.cube_back.ngon_A_n[0],
                          self.cube_back.ngon_A_n[3])

    @property
    def cube_faces(self):
        return [self.cube_front, self.cube_top, self.cube_right, self.cube_bottom, self.cube_left, self.cube_back]

    @property
    def cube_sides(self):
        side_list = list()

        for side in self.cube_front.ngon_sides:
            side_list.append(side)

        for side in self.cube_back.ngon_sides:
            side_list.append(side)

        side_list.append(GmodA3Side(self.cube_front.ngon_A_n[0], self.cube_back.ngon_A_n[0]))
        side_list.append(GmodA3Side(self.cube_front.ngon_A_n[1], self.cube_back.ngon_A_n[1]))
        side_list.append(GmodA3Side(self.cube_front.ngon_A_n[2], self.cube_back.ngon_A_n[2]))
        side_list.append(GmodA3Side(self.cube_front.ngon_A_n[3], self.cube_back.ngon_A_n[3]))

        return side_list

    @property
    def cube_side_quadrs(self):
        return [side.side_quadrance for side in self.cube_sides]

    @property
    def to_gmod_matrix(self):
        return self.cube_points

    @property
    def dual_platonic_solid(self):
        return GmodA3Octahedron(*[face.quad_center for face in self.cube_faces])

    @property
    def inscribed_tetrahedrons(self):
        tetra1 = GmodA3Tetrahedron(self.cube_points[0],
                                   self.cube_points[2],
                                   self.cube_points[5],
                                   self.cube_points[7])
        tetra2 = GmodA3Tetrahedron(self.cube_points[1],
                                   self.cube_points[3],
                                   self.cube_points[4],
                                   self.cube_points[6])
        return [tetra1, tetra2]

    def __plot_on_3d__(self, ax, style):
        for side in self.cube_sides:
            side.__plot_on_3d__(ax, style)

    def __plot_on_xy__(self, ax, style):
        for side in self.cube_sides:
            side.__plot_on_xy__(ax, style)

    def __plot_on_yz__(self, ax, style):
        for side in self.cube_sides:
            side.__plot_on_yz__(ax, style)

    def __plot_on_xz__(self, ax, style):
        for side in self.cube_sides:
            side.__plot_on_xz__(ax, style)

    @property
    def __printdict__(self):
        self_dict = dict()

        self_dict["points"] = [point.__strrepr__ for point in self.cube_points]
        self_dict["Q"] = [quadr.__strrepr__ for quadr in self.cube_side_quadrs]
        self_dict["front"] = self.cube_front.__printdict__
        self_dict["back"] = self.cube_back.__printdict__
        self_dict["top"] = self.cube_top.__printdict__
        self_dict["left"] = self.cube_left.__printdict__
        self_dict["bottom"] = self.cube_bottom.__printdict__
        self_dict["right"] = self.cube_right.__printdict__

        return self_dict

    @staticmethod
    def make_with_dims(corner, width, height, length):
        assert isinstance(corner, GmodA3Point)
        width = Gmod(width)
        height = Gmod(height)
        length = Gmod(length)

        p11 = GmodA3Point(corner.x,
                          corner.y,
                          corner.z)

        p12 = GmodA3Point(corner.x + width,
                          corner.y,
                          corner.z)

        p13 = GmodA3Point(corner.x + width,
                          corner.y + height,
                          corner.z)

        p14 = GmodA3Point(corner.x,
                          corner.y + height,
                          corner.z)

        p21 = GmodA3Point(corner.x,
                          corner.y,
                          corner.z + length)

        p22 = GmodA3Point(corner.x + width,
                          corner.y,
                          corner.z + length)

        p23 = GmodA3Point(corner.x + width,
                          corner.y + height,
                          corner.z + length)

        p24 = GmodA3Point(corner.x,
                          corner.y + height,
                          corner.z + length)

        return GmodA3Cube(p11, p12, p13, p14, p21, p22, p23, p24)

    @staticmethod
    def make_with_tetrahedrons(tetra1, tetra2):
        assert isinstance(tetra1, GmodA3Tetrahedron) and isinstance(tetra2, GmodA3Tetrahedron)

        return GmodA3Cube(tetra1.tetrahedron_points[0],
                          tetra2.tetrahedron_points[0],
                          tetra1.tetrahedron_points[1],
                          tetra2.tetrahedron_points[1],
                          tetra2.tetrahedron_points[2],
                          tetra1.tetrahedron_points[2],
                          tetra2.tetrahedron_points[3],
                          tetra1.tetrahedron_points[3])


UNIT_CUBE = GmodA3Cube.make_with_dims(ORIGIN_3D, 1, 1, 1)
A_PAPER_QUAD = GmodA3Quad(UNIT_CUBE.cube_points[0],
                          UNIT_CUBE.cube_points[2],
                          UNIT_CUBE.cube_points[6],
                          UNIT_CUBE.cube_points[4])


class GmodA3Octahedron(GmodA3GeometryObject, GmodA3PlatonicSolid):

    @property
    def VERTEX_COUNT(self):
        return 6

    @property
    def EDGE_COUNT(self):
        return 12

    @property
    def FACE_COUNT(self):
        return 8

    def __init__(self, *args):
        assert len(args) == self.VERTEX_COUNT
        for point in args:
            assert isinstance(point, GmodA3Point)
        self.octahedron_points = args

    @property
    def octa_inner_quad_points(self):
        return [self.octahedron_points[1], self.octahedron_points[2], self.octahedron_points[3],
                self.octahedron_points[4]]

    @property
    def octa_outer_quad_points(self):
        return [self.octahedron_points[0], self.octahedron_points[5]]

    @property
    def octahedron_sides(self):
        oct_sides = list()
        for oct_vert in self.octa_outer_quad_points:
            for oct_quad in self.octa_inner_quad_points:
                new_side = GmodA3Side(oct_vert, oct_quad)
                oct_sides.append(new_side)

        oct_sides.append(GmodA3Side(self.octa_inner_quad_points[0], self.octa_inner_quad_points[1]))
        oct_sides.append(GmodA3Side(self.octa_inner_quad_points[1], self.octa_inner_quad_points[2]))
        oct_sides.append(GmodA3Side(self.octa_inner_quad_points[2], self.octa_inner_quad_points[3]))
        oct_sides.append(GmodA3Side(self.octa_inner_quad_points[3], self.octa_inner_quad_points[0]))

        return oct_sides

    @property
    def octahedron_side_quadrs(self):
        return [side.side_quadrance for side in self.octahedron_sides]

    @property
    def octahedron_faces(self):
        return [GmodA3Triangle(self.octa_outer_quad_points[0], self.octa_inner_quad_points[0],
                               self.octa_inner_quad_points[1]),
                GmodA3Triangle(self.octa_outer_quad_points[0], self.octa_inner_quad_points[1],
                               self.octa_inner_quad_points[2]),
                GmodA3Triangle(self.octa_outer_quad_points[0], self.octa_inner_quad_points[2],
                               self.octa_inner_quad_points[3]),
                GmodA3Triangle(self.octa_outer_quad_points[0], self.octa_inner_quad_points[3],
                               self.octa_inner_quad_points[0]),
                GmodA3Triangle(self.octa_outer_quad_points[1], self.octa_inner_quad_points[0],
                               self.octa_inner_quad_points[1]),
                GmodA3Triangle(self.octa_outer_quad_points[1], self.octa_inner_quad_points[1],
                               self.octa_inner_quad_points[2]),
                GmodA3Triangle(self.octa_outer_quad_points[1], self.octa_inner_quad_points[2],
                               self.octa_inner_quad_points[3]),
                GmodA3Triangle(self.octa_outer_quad_points[1], self.octa_inner_quad_points[3],
                               self.octa_inner_quad_points[0])]

    @property
    def to_gmod_matrix(self):
        return self.octahedron_points

    @property
    def dual_platonic_solid(self):
        return GmodA3Cube(*[face.triangle_centroid for face in self.octahedron_faces])

    def __plot_on_3d__(self, ax, style):
        for side in self.octahedron_sides:
            side.__plot_on_3d__(ax, style)

    def __plot_on_xy__(self, ax, style):
        for side in self.octahedron_sides:
            side.__plot_on_xy__(ax, style)

    def __plot_on_yz__(self, ax, style):
        for side in self.octahedron_sides:
            side.__plot_on_yz__(ax, style)

    def __plot_on_xz__(self, ax, style):
        for side in self.octahedron_sides:
            side.__plot_on_xz__(ax, style)

    @staticmethod
    def octahedron_from_point_permutations(point):
        assert isinstance(point, GmodA3Point)
        return GmodA3Octahedron(*point.point_permutations)

    @property
    def __printdict__(self):
        self_dict = dict()

        self_dict["octahedron_points"] = [point.__strrepr__ for point in self.octahedron_points]
        self_dict["Q"] = [quadr.__strrepr__ for quadr in self.octahedron_side_quadrs]

        return self_dict


class GmodA3Dodecahedron(GmodA3GeometryObject, GmodA3PlatonicSolid):

    @property
    def VERTEX_COUNT(self):
        return 20

    @property
    def EDGE_COUNT(self):
        return 30

    @property
    def FACE_COUNT(self):
        return 12

    def __init__(self, *args):
        assert len(args) == GmodA3Dodecahedron.VERTEX_COUNT
        for point in args:
            assert isinstance(point, GmodA3Point)
        self.dodecahedron_points = args

    @property
    def to_gmod_matrix(self):
        return self.dodecahedron_points

    @property
    def dual_platonic_solid(self):
        return None

    def __plot_on_3d__(self, ax, style):
        pass

    def __plot_on_xy__(self, ax, style):
        pass

    def __plot_on_yz__(self, ax, style):
        pass

    def __plot_on_xz__(self, ax, style):
        pass

    @property
    def __printdict__(self):
        self_dict = dict()

        self_dict["points"] = [point.__strrepr__ for point in self.dodecahedron_points]

        return self_dict


class GmodA3Icosahedron(GmodA3GeometryObject, GmodA3PlatonicSolid):

    @property
    def VERTEX_COUNT(self):
        return 12

    @property
    def EDGE_COUNT(self):
        return 30

    @property
    def FACE_COUNT(self):
        return 20

    def __init__(self, *args):
        assert len(args) == GmodA3Icosahedron.VERTEX_COUNT
        for point in args:
            assert isinstance(point, GmodA3Point)
        self.icosahedron_points = args

    @property
    def to_gmod_matrix(self):
        return self.icosahedron_points

    def __plot_on_3d__(self, ax, style):
        pass

    def __plot_on_xy__(self, ax, style):
        pass

    def __plot_on_yz__(self, ax, style):
        pass

    def __plot_on_xz__(self, ax, style):
        pass

    @property
    def dual_platonic_solid(self):
        return None

    @property
    def __printdict__(self):
        self_dict = dict()

        self_dict["points"] = [point.__strrepr__ for point in self.icosahedron_points]

        return self_dict


class GmodA3Subplot:

    def __init__(self, ax, xlabel="x", ylabel="y", zlabel="z"):
        self.ax = ax
        self.plotted_geoms = list()

        GmodA3Subplot.add_3d_gridlines(self.ax, xlabel, ylabel, zlabel)

    def plot(self, geom_object, style=False):
        if not style:
            style = "ko"

        if isinstance(geom_object, GmodA3GeometryObject):
            geom_object.__plot_on_3d__(self, style)
            self.plotted_geoms.append(geom_object)
            print(geom_object)
        elif isinstance(geom_object, list):
            for geom in geom_object:
                self.plot(geom, style)
        else:
            assert isinstance(geom_object, GmodA3GeometryObject)

    def read_azim_and_elev(self):
        return [self.ax.azim, self.ax.elev]

    @staticmethod
    def add_3d_gridlines(ax, xlabel, ylabel, zlabel):
        ax.set_xlim([0, 29])
        ax.set_ylim([0, 29])
        ax.set_zlim([0, 29])
        ax.set_xlabel(xlabel)
        ax.set_ylabel(ylabel)
        ax.set_zlabel(zlabel)
        # Don't allow the axis to be on top of your data
        ax.set_axisbelow(True)

        # Turn on the minor TICKS, which are required for the minor GRID
        ax.minorticks_on()

        # Customize the major grid
        ax.grid(which='major', linestyle='-', linewidth='0.5', color='red')
        # Customize the minor grid
        ax.grid(which='minor', linestyle=':', linewidth='0.5', color='black')
        # Turn off the display of all ticks.
        # ax.tick_params(which='both',  # Options for both major and minor ticks
        #              top=False,  # turn off top ticks
        #             left=False,  # turn off left ticks
        #            right=False,  # turn off right ticks
        #           bottom=False)  # turn off bottom ticks
        ax.view_init(azim=-115, elev=17)


class GmodA3Figure:
    plotted_geoms = list()

    def __init__(self, title=None):
        self.figure = plt.figure(GmodA2Figure.FIGURE_COUNT, figsize=(11, 8))
        GmodA2Figure.FIGURE_COUNT += 1

        gs = self.figure.add_gridspec(3, 4)
        self.ax_xy = GmodA2Subplot(self.figure.add_subplot(gs[0, 0], adjustable='box', aspect=1.0),
                                   "x", "y")
        self.ax_yz = GmodA2Subplot(self.figure.add_subplot(gs[1, 0], adjustable='box', aspect=1.0),
                                   "y", "z")
        self.ax_xz = GmodA2Subplot(self.figure.add_subplot(gs[2, 0], adjustable='box', aspect=1.0),
                                   "x", "z")
        self.ax_3d = GmodA3Subplot(self.figure.add_subplot(gs[0:, 1:], projection='3d', adjustable='box', aspect=1.0))

        if title is not None:
            self.figure.suptitle(title)

        self.figure.tight_layout()

    def plot(self, geom_object, style=False):
        if not style:
            style = "ro"

        if isinstance(geom_object, GmodA3GeometryObject):
            geom_object.__plot_on_3d__(self.ax_3d.ax, style)
            geom_object.__plot_on_xy__(self.ax_xy, style)
            geom_object.__plot_on_yz__(self.ax_yz, style)
            geom_object.__plot_on_xz__(self.ax_xz, style)
            print(geom_object)
        elif isinstance(geom_object, list):
            for geom in geom_object:
                self.plot(geom, style)
        elif isinstance(geom_object, dict):
            for key, geom in geom_object.items():
                self.plot(geom, style)
        else:
            print(type(geom_object))
            assert isinstance(geom_object, GmodA3GeometryObject)
