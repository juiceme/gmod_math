
import gmod_numtheory
import gmod_trig
from liber_primus.seg_13_1_constants import *

POLY_BANK = dict()


# POLY_BANK["GR5"] = GmodPoly.poly_with_solutions(5)
# POLY_BANK["GR23"] = GmodPoly.poly_with_solutions(23)
# POLY_BANK["GR6"] = GmodPoly.poly_with_solutions(6)
# POLY_BANK["GR24"] = GmodPoly.poly_with_solutions(24)
# POLY_BANK["I12"] = GmodPoly.poly_with_solutions(12)
# POLY_BANK["I17"] = GmodPoly.poly_with_solutions(17)
# POLY_BANK["FOUND"] = GmodPoly(FOUND)
# POLY_BANK["FOUNDR"] = GmodPoly(FOUND[::-1])
# POLY_BANK["SEEK"] = GmodPoly(SEEK)
# POLY_BANK["SEEKR"] = GmodPoly(SEEK[::-1])
# POLY_BANK["YOU"] = GmodPoly(YOU)
# POLY_BANK["YOUR"] = GmodPoly(YOU[::-1])





def test_all_playgrounds():
    test_print()
    test_registry()

# def test_poly():
#     def print_factoring(poly):
#         print()
#         print(poly)
#         print(poly.poly_factors)
#         print()
#     pprint.pprint(list(zip(POLY_BANK.keys(), POLY_BANK.values())), width=1)
#     print_factoring(POLY_BANK["FOUND"])
#     print_factoring(POLY_BANK["FOUNDR"])
#     print_factoring(POLY_BANK["SEEK"])
#     print_factoring(POLY_BANK["SEEKR"])
#     print_factoring(POLY_BANK["YOU"])
#     print_factoring(POLY_BANK["YOUR"])
#
#
# def irr_info(gmod):
#     gmod = Gmod(gmod)
#     print(gmod, ":", Gmod(gmod))
#     print()
#
#     print(GmodMath.i_eval_mat(gmod).transpose())
#     for i_mat in [i_mat_with for i_mat_with in GmodMath.I_MATS
#                   if gmod in i_mat_with and gmod != i_mat_with[0]]:
#         print(":::", i_mat)
#     print()
#
#     print(GmodMath.phi_eval_mat(gmod).transpose())
#     for phi_val in [phi_mat_val for phi_mat_val in GmodMath.phi_mat(gmod)
#                     if phi_mat_val != gmod]:
#         print(":::", GmodMath.phi_mat(phi_val))
#     print()
#
#
# def irr_info2(gmod):
#     gmod = Gmod(gmod)
#
#     print(gmod, ":", Gmod(gmod))
#     print()
#     print(GmodMath.i_eval_mat(gmod).transpose())
#     for i_mat in [i_mat_with for i_mat_with in GmodMath.I_MATS
#                   if gmod in i_mat_with and gmod != i_mat_with[0]]:
#         print(":::", i_mat)
#     print()
#     print(GmodMath.phi_eval_mat(gmod).transpose())
#     for phi_mat in [phi_mat_with for phi_mat_with in GmodMath.PHI_MATS
#                     if gmod in phi_mat_with and gmod != phi_mat_with[0]]:
#         print(":::", phi_mat)
#     print()
#     # print("%s // %s == (%s + %s) // %s " % (gmod, phi_repr[1], phi_repr[1], gmod, gmod))
#     # print("%s // %s == (%s + %s) // %s " % (gmod, phi_repr[2], phi_repr[2], gmod, gmod))
#     # print("%s // %s == (%s - %s) // %s " % (gmod, phi_repr[3], phi_repr[3], gmod, gmod))
#     # print("%s // %s == (%s - %s) // %s " % (gmod, phi_repr[4], phi_repr[4], gmod, gmod))
#     # print(":")
#     print(GmodMath.gmod_phi_triangle(gmod))


#
# def old_main():
#     TEST_POINT1 = GmodA2Point(25, 24)
#     TEST_POINT2 = GmodA2Point(1, 1)
#     TEST_POINT3 = GmodA2Point(24, 25)
#     TEST_TRI1 = GmodA2Triangle(TEST_POINT1, TEST_POINT2, TEST_POINT3)
#     TEST_345_TRI = GmodA2Triangle(GmodA2Point(0, 0), GmodA2Point(3, 0), GmodA2Point(3, 4))
#
#     P3_POINT = GmodA3Point(25, 10, 19)
#     C3_POINT = GmodA3Point(24, 13, 15)
#     K3_POINT = GmodA3Point(28, 3, 25)
#     NEGK3_POINT = GmodA3Point(1, 26, 4)
#     YOU_POINT = GmodA3Point(27, 4, 2)
#     PKC_POINT = GmodA3Point(25, 28, 24)
#     PCK_POINT = GmodA3Point(25, 24, 28)
#     TEST_P1_POINT_x = GmodA3Point(25, 0, 0)
#     TEST_P1_POINT_xy = GmodA3Point(25, 25, 0)
#     TEST_P1_POINT_xyz = GmodA3Point(25, 25, 25)
#
#     TRI1 = GmodA3Triangle(GmodA3Point(25, 10, 19),
#                           GmodA3Point(28, 3, 25),
#                           GmodA3Point(24, 13, 15))
#
#     TRI2 = GmodA3Triangle(GmodA3Point(25, 10, 19),
#                           GmodA3Point(24, 13, 15),
#                           GmodA3Point(1, 26, 4))
#
#     TRI3 = GmodA3Triangle(GmodA3Point(25, 24, 28),
#                           GmodA3Point(10, 13, 3),
#                           GmodA3Point(19, 15, 25))
#
#     TRI4 = GmodA3Triangle(GmodA3Point(25, 24, 1),
#                           GmodA3Point(10, 13, 26),
#                           GmodA3Point(19, 15, 4))
#
#     QUAD1 = GmodA3Quad(GmodA3Point(25, 10, 19),
#                        GmodA3Point(28, 3, 25),
#                        GmodA3Point(24, 13, 15),
#                        GmodA3Point(1, 26, 4))
#
#     QUAD2 = GmodA3Quad(GmodA3Point(25, 24, 28),
#                        GmodA3Point(10, 13, 3),
#                        GmodA3Point(19, 15, 25),
#                        GmodA3Point(14, 6, 21))
#
#     QUAD3 = GmodA3Quad(GmodA3Point(25, 24, 1),
#                        GmodA3Point(10, 13, 26),
#                        GmodA3Point(19, 15, 4),
#                        GmodA3Point(14, 6, 8))
#
#     def a2_circle_test1():
#         TEST_PLOT_CIRCS1 = GmodA2Figure()
#         TEST_PLOT_CIRCS1.plot(GmodA2Circle.UNIT_CIRCLE, "bo")
#         TEST_PLOT_CIRCS1.plot(GmodA2Circle.PROJECTIVE_CIRCLE, "go")
#         TEST_PLOT_CIRCS1.plot(GmodA2Conic(1, 0, 1, 28, 0, 0), "ro")
#         TEST_PLOT_CIRCS1.plot(GmodA2Conic(1, 0, 1, 28, 0, 28), "b+")
#         TEST_PLOT_CIRCS1.plot(GmodA2Conic(1, 0, 1, 28, 28, 0), "g+")
#         TEST_PLOT_CIRCS1.plot(GmodA2Conic(1, 0, 1, 0, 28, 28), "k+")
#         TEST_PLOT_CIRCS1.plot(GmodA2Conic(1, 0, 1, 28, 28, 28), "m+")
#
#         TEST_PLOT_CIRCS2 = GmodA2Figure()
#         TEST_PLOT_CIRCS2.plot(GmodA2Conic(1, 0, 1, 28, 28, 0), "go")
#         TEST_PLOT_CIRCS2.plot(GmodA2Conic(1, 0, 1, 28, 28, 28), "mo")
#
#     def a2_triangle_test1():
#         TEST_PLOT2D = GmodA2Figure()
#         TEST_PLOT2D.plot(TEST_TRI1, "ro")
#         TEST_PLOT2D.plot(TEST_345_TRI, "mo")
#         TEST_PLOT2D.plot(GmodA2Circle.UNIT_CIRCLE, "bo")
#         TEST_PLOT2D.plot(GmodA2Circle.PROJECTIVE_CIRCLE, "bo")
#         TEST_PLOT2D.plot(GmodA2Conic(1, 0, 1, 28, 0, 0), "bo")
#
#     def a2_triangle_test2():
#         TEST_PLOT2 = GmodA3Figure()
#         TEST_PLOT2.plot(TRI1, "go")
#         TEST_PLOT2.plot(TRI2, "bo")
#         TEST_PLOT2.plot(TRI3, "ro")
#         TEST_PLOT2.plot(TRI4, "mo")
#
#     def a2_triangle_test3():
#         TEST_PLOT2 = GmodA3Figure()
#         TEST_PLOT2.plot(TRI4, "mo")
#         TEST_PLOT2.plot(TRI2, "mo")
#
#         TEST_PLOT2.plot(QUAD1, "go")
#         TEST_PLOT2.plot(QUAD2, "bo")
#         TEST_PLOT2.plot(QUAD3, "ro")
#
#     def a2_logistic_map_test():
#         TEST_PLOT2 = GmodA2Figure()
#         TEST_PLOT2.plot(GmodA2Parabola.CHAOS_THEORY_PARAB, "mo")
#
#     def a3_unit_cube_test1():
#         TEST_PLOT = GmodA3Figure()
#         TEST_PLOT.plot(GmodA3Cube.UNIT_CUBE, "r+")
#         TEST_PLOT.plot(GmodA3Cube.UNIT_CUBE.dual_platonic_solid, "bo")
#
#     def a3_test1():
#         test_cube = GmodA3Cube.make_with_dims(GmodA3Point(12, 12, 12), 6, 6, 6)
#         test_octa = test_cube.dual_platonic_solid
#         test_tetra1 = test_cube.inscribed_tetrahedrons[0]
#         test_tetra2 = test_cube.inscribed_tetrahedrons[1]
#
#         TEST_PLOT = GmodA3Figure()
#         TEST_PLOT.plot(test_cube, "r+")
#         TEST_PLOT.plot(test_octa, "b+")
#         TEST_PLOT.plot(test_tetra1, "m+")
#         TEST_PLOT.plot(test_tetra2, "g+")
#
#     def octahedron_test1():
#         plot1 = GmodA3Figure("P3, C3 Permutation Octahedrons")
#         plot1.plot(GmodA3Octahedron.octahedron_from_point_permutations(GmodA3Point(25, 10, 19)), "go")
#         plot1.plot(GmodA3Octahedron.octahedron_from_point_permutations(GmodA3Point(24, 13, 15)), "ro")
#
#         plot2 = GmodA3Figure("K3, NK3 Permutation Octahedrons")
#         plot2.plot(GmodA3Octahedron.octahedron_from_point_permutations(GmodA3Point(28, 3, 25)), "mo")
#         plot2.plot(GmodA3Octahedron.octahedron_from_point_permutations(GmodA3Point(1, 26, 4)), "go")
#
#         plot3 = GmodA3Figure("PCK, PCNK Permutation Octahedrons")
#         plot3.plot(GmodA3Octahedron.octahedron_from_point_permutations(GmodA3Point(24, 25, 28)), "co")
#         plot3.plot(GmodA3Octahedron.octahedron_from_point_permutations(GmodA3Point(24, 25, 1)), "mo")
#
#         plot4 = GmodA3Figure("PCK, PCNK Permutation Octahedrons")
#         plot4.plot(GmodA3Octahedron.octahedron_from_point_permutations(GmodA3Point(25, 10, 19)).dual_platonic_solid,
#                    "bo")
#         plot4.plot(GmodA3Octahedron.octahedron_from_point_permutations(GmodA3Point(24, 13, 15)).dual_platonic_solid,
#                    "ro")
#         plot4.plot(GmodA3Octahedron.octahedron_from_point_permutations(GmodA3Point(28, 3, 25)).dual_platonic_solid,
#                    "co")
#         plot4.plot(GmodA3Octahedron.octahedron_from_point_permutations(GmodA3Point(1, 26, 4)).dual_platonic_solid, "mo")
#
#     def my_tab(tabsize=0):
#         count = 0
#         tabstr = ""
#         while count < tabsize:
#             tabstr += "\t"
#             count += 1
#         return tabstr
#
#     def print_operation_informtion(gmod1, gmod2, tabs=0):
#         gmod1 = Gmod(gmod1)
#         gmod2 = Gmod(gmod2)
#
#         operation_dict = dict()
#         operation_dict["x = "] = str(gmod1)
#         operation_dict["y = "] = str(gmod2)
#         operation_dict["x+y = "] = str(gmod1 + gmod2)
#         operation_dict["x-y = "] = str(gmod1 - gmod2)
#         operation_dict["y-x ="] = str(gmod2 - gmod1)
#         operation_dict["x*y = "] = str(gmod1 * gmod2)
#         operation_dict["x//y = "] = str(gmod1 // gmod2)
#         operation_dict["y//x = "] = str(gmod2 // gmod1)
#         operation_dict["x**y = "] = str(gmod1 ** gmod2)
#         operation_dict["y**x = "] = str(gmod2 ** gmod1)
#         operation_dict["x**z=y: "] = str(GmodLogarithm.revlog(gmod1, gmod2))
#         operation_dict["y**z=x: "] = str(GmodLogarithm.revlog(gmod2, gmod1))
#         operation_dict["z**x=y: "] = str(GmodLogarithm.logbase(gmod1, gmod2))
#         operation_dict["z**y=x: "] = str(GmodLogarithm.logbase(gmod2, gmod1))
#
#         # for key, value in operation_dict.items():
#         print(json.dumps(operation_dict, indent=tabs))
#
#         return operation_dict
#
#     def print_phi_information(gmod, tabs=0):
#         gmod = Gmod(gmod)
#
#         phi_dict = dict()
#         phi_dict["x: "] = gmod
#         phi_dict["phi_mat: "] = GoldenRatio.phi_mats[gmod]
#         phi_dict["lin_trans: "] = GoldenRatio.phi_trans_mats[gmod]
#         for key, value in phi_dict.items():
#             print(key, value)
#
#         return phi_dict
#
#     def print_single_information(gmod, tabs=0):
#         gmod = Gmod(gmod)
#         print(my_tab(tabs), "GmodInformation:", str(gmod))
#
#         print(my_tab(tabs + 1), "Exp:", str(gmod))
#
#         for mulmod in Gmod.ALL_GMODS:
#             mul = gmod ** mulmod
#             print(my_tab(tabs + 2), str(mulmod), ":", str(mul))
#
#         for expmod in Gmod.ALL_GMODS:
#             exp = gmod ** expmod
#             print(my_tab(tabs + 2), str(expmod), ":", str(exp))
#
#         print(my_tab(tabs + 1), "GoldenRatioInformation:", str(gmod))
#
#         print(my_tab(tabs + 2), "phi1:", str(GoldenRatio.phi1))
#         print_operation_informtion(gmod, GoldenRatio.phi1, tabs + 3)
#
#         print(my_tab(tabs + 2), "phi2", str(GoldenRatio.phi2))
#         print_operation_informtion(gmod, GoldenRatio.phi2, tabs + 3)
#
#         print(my_tab(tabs + 2), "Phi1:", str(GoldenRatio.Phi1))
#         print_operation_informtion(gmod, GoldenRatio.Phi1, tabs + 3)
#
#         print(my_tab(tabs + 2), "Phi2:", str(GoldenRatio.Phi2))
#         print_operation_informtion(gmod, GoldenRatio.Phi2, tabs + 3)
#
#         print(my_tab(tabs + 2), "psi:", str(GoldenRatio.psi))
#         print_operation_informtion(gmod, GoldenRatio.psi, tabs + 3)
#
#     def print_relation_information(gmod1, gmod2, tabs=0):
#         print(my_tab(tabs), "Relations:", gmod1, gmod2)
#         print_operation_informtion(gmod1, gmod2, tabs + 1)
#
#     def main_information(gmod1, gmod2, tabs=0):
#         gmod1 = Gmod(gmod1)
#         gmod2 = Gmod(gmod2)
#         print()
#         print_relation_information(gmod1, gmod2, tabs)
#         print(), print()
#         print_single_information(gmod1, tabs)
#         print(), print()
#         print_single_information(gmod2, tabs)
#         print()
#
#     print_operation_informtion(Gmod(7), Gmod(20))
#     print()
#     print_operation_informtion(Gmod(23), Gmod(24))
#     print()
#
#     print_operation_informtion(Gmod(7), Gmod(25))
#     print()
#     print_operation_informtion(Gmod(7), Gmod(23))
#     print()
#
#     def print_complex_analysis(gmod1, gmod2):
#         gmod1 = Gmod(gmod1)
#         gmod2 = Gmod(gmod2)
#
#         complex_anal = dict()
#         complex_anal["x: "] = gmod1
#         complex_anal["y: "] = gmod2
#         complex_anal["x(i): "] = GmodImaginary.gmod_i_repr(gmod1)
#         complex_anal["y(i): "] = GmodImaginary.gmod_i_repr(gmod2)
#         complex_anal["x(y): "] = GmodImaginary.gmod_i_repr_with_base(gmod1, gmod2)
#         complex_anal["y(x): "] = GmodImaginary.gmod_i_repr_with_base(gmod2, gmod1)
#         complex_anal["x(y)i: "] = GmodImaginary.gmod_i_repr_with_i_base(gmod1, gmod2)
#         complex_anal["y(x)i: "] = GmodImaginary.gmod_i_repr_with_i_base(gmod2, gmod1)
#
#         for key, value in complex_anal.items():
#             print(key, value)
#
#         # return complex_anal
#
#     def info_matrix():
#         rows = list()
#         for gmod in Gmod.ALL_GMODS:
#             inverse_mat = GmodMatrix.INVERSE_MATRIX[gmod]
#             square_mat = GmodMatrix.SQUARE_MATRIX[gmod]
#             phi_mat = GoldenRatio.PHI_MATS[gmod]
#             trans_mat = GoldenRatio.PHI_TRANS_MATS[gmod]
#
#             row = GmodStruct(gmod,
#                              *inverse_mat[1::],
#                              *square_mat[1::],
#                              *phi_mat[1::],
#                              *trans_mat[1::])
#             rows.append(row)
#         return GmodMatrix(rows)

my_playground.test_all_playgrounds()

"""
Notes:

GmodA3Point(25, 10, 19)
GmodA3Point(24, 13, 15)
GmodA3Point(28, 3, 25)
GmodA3Point(1, 26, 4)
GmodA3Point(27, 4, 2)

In [39]: GmodA2Point(25,10).as_vector.quadrance                       
Out[39]: [0 % EA]

In [40]: GmodA2Point(24,13).as_vector.quadrance                       
Out[40]: [20 % M]

In [41]: GmodA2Point(1,26).as_vector.quadrance                        
Out[41]: [10 % N]

In [42]: GmodA2Point(28,3).as_vector.quadrance                        
Out[42]: [10 % N]


In [44]: GmodA2Point(25,1).as_vector.quadrance                        
Out[44]: [17 % T]

In [45]: GmodA2Point(25,24).as_vector.quadrance                       
Out[45]: [12 % J]

In [46]: GmodA2Point(1,24).as_vector.quadrance                        
Out[46]: [26 % AE]

In [47]: GmodA2Point(1,28).as_vector.quadrance                        
Out[47]: [2 % U]




In [56]: GmodA3Point(25, 10, 19).as_vector.quadrance                  
Out[56]: [13 % EO]

In [57]: GmodA3Point(24, 13, 15).as_vector.quadrance                  
Out[57]: [13 % EO]

In [58]: GmodA3Point(1, 26, 4).as_vector.quadrance                    
Out[58]: [26 % AE]

In [59]: GmodA3Point(28, 3, 25).as_vector.quadrance                   
Out[59]: [26 % AE]


In [60]: GmodA3Point(28, 24, 25).as_vector.quadrance                  
Out[60]: [13 % EO]









In [52]: GmodA3Point.quadrance_between(GmodA3Point(25, 10, 19), GmodA3Point(1, 26, 4))                                                           
Out[52]: [13 % EO]

In [53]: GmodA3Point.quadrance_between(GmodA3Point(28, 3, 25), GmodA3Point(24, 13, 15))                                                         
Out[53]: [13 % EO]

In [54]: GmodA3Point.quadrance_between(GmodA3Point(24, 13, 15), GmodA3Point(1, 26, 4))                                                                            
Out[54]: [7 % G]

In [55]: GmodA3Point.quadrance_between(GmodA3Point(25, 10, 19), GmodA3Point(28, 3, 25))                                                                                  
Out[55]: [7 % G]


In [48]: GmodA3Point.quadrance_between(GmodA3Point(27, 4, 2), GmodA3Point(25, 10, 19))                                                          
Out[48]: [10 % N]

In [49]: GmodA3Point.quadrance_between(GmodA3Point(27, 4, 2), GmodA3Point(24, 13, 15))                                                          
Out[49]: [27 % Y]

In [50]: GmodA3Point.quadrance_between(GmodA3Point(27, 4, 2), GmodA3Point(28, 3, 25))                                                           
Out[50]: [9 % H]

In [51]: GmodA3Point.quadrance_between(GmodA3Point(27, 4, 2), GmodA3Point(1, 26, 4))                                                            
Out[51]: [4 % O]



"""

# PLOT1.plot(GmodHLine(19,23,2)
"""
PLOT1.plot(GmodHSide(GmodHPoint(10,15,21), GmodHPoint(23,17,8)), "bo")
PLOT1.plot(GmodHSide(GmodHPoint(25,28,24), GmodHPoint(24,1,25)), "go")

PLOT1.plot(GmodHSide(GmodHPoint(25,28,24), GmodHPoint(10,15,21)), "m+")
PLOT1.plot(GmodHSide(GmodHPoint(25,28,24), GmodHPoint(23,17,8)), "m+")
PLOT1.plot(GmodHSide(GmodHPoint(24,1,25), GmodHPoint(10,15,21)), "m+")
PLOT1.plot(GmodHSide(GmodHPoint(24,1,25), GmodHPoint(23,17,8)), "m+")

PLOT1.plot(GmodHTriangle(GmodHPoint(25, 0, 0), GmodHPoint(0, 28, 0), GmodHPoint(0, 0, 24)))
PLOT1.plot(GmodHTriangle(GmodHPoint(1, 0, 0), GmodHPoint(0, 24, 0), GmodHPoint(0, 0, 25)))


COLINE = GmodHPoint(25,10,19).colinear_line_for_points(GmodHPoint(24,13,15), GmodHPoint(28,3,25))
"""
# PLOT1.plot(CO_LINE, "g+")
