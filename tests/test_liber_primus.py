import gmod_trig
from liber_primus.seg_13_1_constants import *
from gmod_numtheory import *


def test_word_matrix():
    print_separator()
    print(CIPHER_REG["word_matrix"]), print_separator()
    print_separator()


def test_imag():
    print_separator()

    def make_imag_2d(reg, matrixkey, index1, index2):
        return GmodMatrix([[Gmod(row[index1], row[index2]),
                            Gmod(row[index1], row[index2]).ispace12,
                            Gmod(row[index1], row[index2]).ispace17] for row in reg[matrixkey].transpose()])

    def pc_imag(reg):
        return make_imag_2d(reg, "word_matrix", 0, 1)

    def cp_imag(reg):
        return make_imag_2d(reg, "word_matrix", 1, 0)

    CIPHER_REG.register_function(pc_imag)
    CIPHER_REG.register_function(cp_imag)

    print(CIPHER_REG["pc_imag"]), print_separator()
    print(CIPHER_REG["cp_imag"]), print_separator()
    print_separator()


def test_euler():
    print_separator()

    def make_euler(reg, matrixkey):
        euler_list = list()

        for gmod_row in reg[matrixkey]:
            euler_list.append(gmod_trig.euler_for_gmod(gmod_row[0]))

        print(euler_list)

        return GmodMatrix(euler_list)

    def pc_euler(reg):
        return make_euler(reg, "pc_imag")

    def cp_euler(reg):
        return make_euler(reg, "cp_imag")

    # CIPHER_REG.register_function(pc_euler)
    # CIPHER_REG.register_function(cp_euler)

    # print(CIPHER_REG)
